package pl.kits.interfaces;

import pl.kits.classes.Branch;

import java.io.InputStream;
import java.io.OutputStream;

public interface IBranch {

    /**
     * Counts all leaves in this Branch
     *
     * @return amount of leaves under this Branch
     */
    int totalLeafCount();

    /**
     * Returns amount of bytes that Branch consists of (in file)
     *
     * @return n.o. bytes of serialized Branch
     */
    int serializedSize();

    /**
     * Returns amount of bytes that Root consists of (in file)
     *
     * @return n.o. bytes of serialized Root
     */
    int serializedRootSize();

    /**
     * @return roothash of the last Leaf or new byte[32] for empty Leaf list
     */
    byte[] rootHash();

    /**
     * Adds Leaf to branch.
     * Adds given hash and timestamp to a new Leaf and places it into Branch by
     * computing new roothash.
     * New roothash is: SHA-256(old roothash + hash + timestamp)
     *
     * @param hash      hash to be added
     * @param timestamp timestamp to be considered
     * @return current roothash
     * @throws Exception when verify fails
     */
    byte[] addLeaf(byte[] hash, byte[] timestamp) throws Exception;

    /**
     * Adds Branch with given timestamp
     *
     * @param branch    Branch to be added
     * @param timestamp Timestamp to be added
     * @return 32 byte array of new roothash
     * @throws Exception when Branch null or timestamp length inappropiate
     */
    byte[] addBranch(Branch branch, byte[] timestamp) throws Exception;

    /**
     * Add Branch from stream to an existing tree.
     *
     * @param stream data input stream
     * @return new roothash
     * @throws Exception when IO or verify fails
     */
    byte[] addBranch(InputStream stream) throws Exception;


    /**
     * Find a timestamp of stored hash.
     *
     * @param hash hash to be found
     * @return 4 byte array timestamp or byte[0] when could not find
     */
    byte[] find(byte[] hash);

    /**
     * Find Branch of given hash
     *
     * @param hash hash of Branch to be found
     * @return found Branch or new Branch() if not found
     */
    Branch findBranch(byte[] hash);

    /**
     * Find a hash in given Branch and return the
     * Branch that is a root of minimum tree containing that hash
     *
     * @param hash hash to be found
     * @return branch-root of minimum tree containing the hash
     */
    Branch proofFind(byte[] hash) throws Exception;


    /**
     * Serializes the whole Branch to a stream
     *
     * @param stream stream to be written into
     * @throws Exception when serialization impossible
     */
    void serialize(OutputStream stream) throws Exception;

    /**
     * Serializes concrete Branch to a stream
     *
     * @param hash   roothash of a Branch
     * @param stream stream for output
     * @throws Exception when hash length different then 32B
     */
    void serialize(byte[] hash, OutputStream stream) throws Exception;


    /**
     * Serializes concrete Branch from given Branch object to given stream
     *
     * @param hash   roothash of Branch
     * @param branch Branch to be searched in
     * @param stream stream for output
     * @throws Exception when IO fails
     */
    void serialize(byte[] hash, Branch branch, OutputStream stream) throws Exception;

    /**
     * Serializes only a root to stream
     *
     * @param stream stream for output
     * @throws Exception when impossible to be completed
     */
    void serializeRoot(OutputStream stream) throws Exception;


    /**
     * Serializes Branch into a hash
     *
     * @param hash   desired hash
     * @param stream stream for output
     */
    void serializeRootFrom(byte[] hash, OutputStream stream) throws Exception;


    /**
     * Serializes root of Branch of given hash from the main root.
     *
     * @param hash from serialization should be started
     * @param stream stream to be written into
     * @throws Exception when IO failed
     */
    void serializeRoot(byte[] hash, OutputStream stream) throws Exception;

    /**
     * Serializes arbitrary Branch by hash to stream
     *
     * @param hash   roothash of desired Branch
     * @param stream stream for output
     */
    void serializeBranch(byte[] hash, OutputStream stream) throws Exception;

    /**
     * Serializes a Branch with given roothash under a given parent Branch
     *
     * @param hash   Branch roothash
     * @param branch parent Branch to be searched on
     * @param stream stream for output
     */
    void serializeBranch(byte[] hash, Branch branch, OutputStream stream) throws Exception;


    /**
     * Joins root into Branch from input stream. Each new Leaf
     * is verified by hash and if it fails, it stops and returns current
     * roothash.
     * @param stream input stream
     * @return extended Branch roothash or byte[0] when failed
     */
    byte[] joinRoot(InputStream stream) throws Exception;


    /**
     * Verifies the whole Branch
     *
     * @param branch Branch to be verified
     * @return true/false depending on success of verification
     */
    boolean verifyBranch(Branch branch) throws Exception;

    boolean verify() throws Exception;

    /**
     * Verifies hashes in root of the Branch
     *
     * @return true/false depending on verify success/fail
     */
    boolean verifyRoot() throws Exception;

    /**
     * Verifies root which does not start with 0-s
     *
     * @return result of verification
     */
    boolean verifyRootPart();

    /**
     * Get number of root leaves to determine whether it is empty
     *
     * @return number of root leaves
     */
    int numberOfRootLeaves();
}
