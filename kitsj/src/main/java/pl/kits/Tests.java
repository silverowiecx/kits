package pl.kits;

import pl.kits.classes.Branch;
import pl.kits.classes.Pair;
import pl.kits.classes.Timestamp;
import pl.kits.database.BranchDTO;
import pl.kits.database.Connection;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static pl.kits.classes.ByteUtils.*;
import static pl.kits.classes.ByteUtils.unsignedToBytes;
import static pl.kits.classes.Log.log;
import static pl.kits.classes.Log.logError;

public class Tests {
    public static Connection c;

    static Branch root = new Branch();
    static int n = 4;
    static List<Pair<byte[], byte[]>> hasze = new ArrayList<>();
    static List<byte[]> hasze2 = new ArrayList<>();
    static boolean jeden2 = true;
    static boolean jeden1 = true;

    static void searchCheckDB() throws Exception {
        log("Testuje wyszukiwanie w bazie danych...");
        Random r = new Random();
        Connection c = new Connection();
        for (int i = 1; i < 4; i++) {
            int h = r.nextInt(hasze.size());
            byte[] hash = hasze.get(h).getKey();
            BigInteger ts = new BigInteger(hasze.get(h).getValue());
            log(i + " dla:" + bytesToHex(hash) + " szukam:" + ts);
            byte[] t = BranchDTO.find(c, hash);
            BigInteger ts2 = new BigInteger(t);
            log("Dla:" + bytesToHex(hash) + " dostalem: " + ts2);
            log("Weryfikacja Branch po Find poprawna: " + root.verifyBranch(BranchDTO.proofFind(c, hash)));
            if (!ts.equals(ts2)) {
                log(" Fail!");
            } else log(" Pass!");
        }
    }

    static void saveHashes(List<Pair<byte[], byte[]>> hashes, String fileName) throws IOException {
        PrintWriter fw = new PrintWriter(new File(fileName));
        for (Pair<byte[], byte[]> pair : hashes) {
            fw.println(bytesToHex(pair.getKey()));
            fw.println(bytesToHex(pair.getValue()));
        }
    }

    static List<Pair<byte[], byte[]>> loadHashes(String fileName) throws FileNotFoundException {
        ArrayList<Pair<byte[], byte[]>> hashes = new ArrayList<>();
        Scanner sc = new Scanner(new File(fileName));
        while (true) {
            try {
                byte[] h = hexToBytes(sc.nextLine());
                byte[] ts = hexToBytes(sc.nextLine());
                hashes.add(new Pair<>(h, ts));
            } catch (NoSuchElementException e) {
                break;
            }
        }
        sc.close();
        return hashes;
    }

    static Branch deterministic() throws Exception {
        Branch root = new Branch();


        byte[] h;
        byte[] t;
        Random r = new Random();
        byte[] toBig = new byte[32];
        r.nextBytes(toBig);
        BigInteger hash = new BigInteger(toBig);

        long timestamp = 140000;

        for (int i = 0; i < 10; i++) {
            Branch l1 = new Branch();

            for (int j = 0; j < 20; j++) {
                Branch low = new Branch();
                for (int l = 0; l < 100; l++) {
                    hash = hash.subtract(BigInteger.valueOf(1));
                    timestamp++;
                    h = hash.toByteArray();
                    t = unsignedToBytes(timestamp);
                    low.addLeaf(h, t);
                    hasze.add(new Pair<>(h, t));
                }
                timestamp++;
                t = unsignedToBytes(timestamp);
                l1.addBranch(low, t);
            }
            timestamp++;
            t = unsignedToBytes(timestamp);
            root.addBranch(l1, t);
        }

        return root;
    }


    static void moar() throws Exception {
        for (int i = 0; i < 100; i++) {
            System.out.print("\rBranch: " + i);
            root.addBranch(randomBoB(20, n), unsignedToBytes(rts()));
        }
        log("\nHashes: " + hasze.size());
    }

    static void searchCheck() throws Exception {
        log("Testuje wyszukiwanie...");
        Random r = new Random();
        for (int i = 1; i < 4; i++) {
            int h = r.nextInt(hasze.size());
            byte[] hash = hasze.get(h).getKey();
            BigInteger ts = new BigInteger(hasze.get(h).getValue());
            log(i + " dla:" + bytesToHex(hash) + " szukam:" + ts);
            byte[] t = root.find(hasze.get(h).getKey());
            BigInteger ts2 = new BigInteger(t);
            log("Dla:" + bytesToHex(hash) + " dostalem: " + ts2);
            log("Weryfikacja Branch po Find poprawna: " + root.verifyBranch(root.proofFind(hash)));
            log("Sciezka Branchy: " + Branch.path);
            Branch.path.clear();
            if (!ts.equals(ts2)) {
                log(" Fail!");
            } else log(" Pass!");
        }
    }

    static void searchFull() throws Exception {
        int p = 0, q = 0;
        Collections.shuffle(hasze);
        for (int x = 0; x < 1; x++) {
            long time = System.currentTimeMillis();
            for (Pair<byte[], byte[]> pair : hasze) {
                Branch b = root.proofFind(pair.getKey());
                if (!b.verify())
                    p++;
                if (Arrays.equals(b.find(pair.getKey()), pair.getValue()))
                    q++;
                else p++;
                System.out.print("\r Porazki: " + p + " sukcesy: " + q);
            }
            log(" petla 2, czas: " + (System.currentTimeMillis() - time) / 1000);
            p = 0;
            q = 0;
        }
    }

    static void searchFullDb() throws Exception {
        int p = 0, q = 0;
        Collections.shuffle(hasze);
        Connection c = new Connection();
        for (int x = 0; x < 1; x++) {
            long time = System.currentTimeMillis();
            for (Pair<byte[], byte[]> pair : hasze) {
                Branch b = BranchDTO.proofFind(c, pair.getKey());
                if (!b.verify())
                    p++;
                if (Arrays.equals(b.find(pair.getKey()), pair.getValue()))
                    q++;
                else p++;
                System.out.print("\r Porazki: " + p + " sukcesy: " + q);
            }
            log(" petla 2, czas: " + (System.currentTimeMillis() - time) / 1000);
            p = 0;
            q = 0;
        }
    }


    public static long rts() { // random timestamp
        Random r = new Random();
        return bytesToUnsigned(Timestamp.get()) - r.nextInt(1000000000);
    }


    static void testy1() throws Exception {

        int n = 0;
        Random r = new Random();


        log("Letsgo!");

        Branch root = new Branch();


        for (int i = 0; i < 10; i++) {
            log("Galaz drzewa: " + i);
            root.addBranch(randomBranch(20 + i, n), unsignedToBytes(rts()));
        }
        log("Lacznie haszy: " + hasze.size());

        log("Testuje wyszukiwanie...");
        for (int i = 1; i < 4; i++) {
            int h = r.nextInt(hasze.size());
            byte[] hash = hasze.get(h).getKey();
            BigInteger ts = new BigInteger(hasze.get(h).getValue());
            log(i + " dla:" + bytesToHex(hash) + " szukam:" + ts);

            byte[] t = root.find(hash);
            BigInteger ts2 = new BigInteger(t);
            log("Dla:" + bytesToHex(hash) + " dostalem: " + ts2);
            log("Weryfikacja Branch po Find poprawna: " + root.verifyBranch(root.proofFind(hash)));
            log("Sciezka Branchy: " + Branch.path);
            Branch.path.clear();
            if (!ts.equals(ts2)) {
                log(" Fail!");
            } else log(" Pass!");
        }
    }

    static void saveOnes() throws FileNotFoundException {
        for (byte[] hasz : hasze2) {
            String hex = bytesToHex(hasz);
            log("Saving " + hex);
            FileOutputStream fos = new FileOutputStream(new File(hex));
            root.serializeBranch(hasz, fos);
        }
    }


    public static void partialRoot() throws Exception {
        Branch b = new Branch();
        for (int i = 1; i < 10; i++) {
            byte[] rts = unsignedToBytes(rts());
            byte[] hash = new byte[32];
            b.addLeaf(hash, rts);
        }
        FileOutputStream fos = new FileOutputStream(new File("root"));
        b.serializeRoot(fos);
        log("TLC: " + b.totalLeafCount() + " RH: " + bytesToHex(b.rootHash()));
        byte[] root = b.rootHash();
        for (int i = 1; i < 10; i++) {
            byte[] rts = unsignedToBytes(rts());
            byte[] hash = new byte[32];
            b.addLeaf(hash, rts);
        }
        fos = new FileOutputStream(new File("root2"));
        b.serializeRootFrom(root, fos);
        root = b.rootHash();
        log("TLC2: " + b.totalLeafCount() + " RH: " + bytesToHex(root));
        b = new Branch();
        FileInputStream fis = new FileInputStream(new File("root"));
        b = Branch.onlyRoot(fis);
        assert b != null;
        log("TLC: " + b.totalLeafCount() + " RH: " + bytesToHex(b.rootHash()));
        fis = new FileInputStream(new File("root2"));
        byte[] rh = b.joinRoot(fis);
        if (!Arrays.equals(rh, root))
            logError("OOPS! FRH: " + bytesToHex(rh) + "; expected: " + bytesToHex(root));
        log("TLC2: " + b.totalLeafCount());
        log(("Weryfikacja root poprawna: " + b.verifyRoot()));
    }

    public static void partialRootDb() throws Exception {
        Branch b = new Branch();
        Random r = new Random();
        for (int i = 1; i < 10; i++) {
            byte[] rts = unsignedToBytes(rts());
            byte[] hash = new byte[32];
            r.nextBytes(hash);
            b.addLeaf(hash, rts);
        }
        Connection c = new Connection();
        BranchDTO.saveTreeObject(c, b);
        c.connection.commit();
        BranchDTO.serializeRoot(c, new FileOutputStream(new File("root3")));
        log("TLC: " + BranchDTO.totalLeafCount(c) + " RH: " + bytesToHex(BranchDTO.getRootHash(c)));
        byte[] root = BranchDTO.getRootHash(c);

        for (int i = 1; i < 10; i++) {
            byte[] rts = unsignedToBytes(rts());
            byte[] hash = new byte[32];
            r.nextBytes(hash);
            BranchDTO.addLeaf(c, hash, rts, null);
        }
        c.connection.commit();
        BranchDTO.serializeRootFrom(c, new FileOutputStream(new File("root4")), root);
        root = BranchDTO.getRootHash(c);
        log("TLC2: " + BranchDTO.totalLeafCount(c) + " RH: " + bytesToHex(root));

        PreparedStatement statement = c.connection.prepareStatement("Delete from Leaf;");

        statement.execute();

        c.connection.commit();

        BranchDTO.joinRoot(c, new FileInputStream(new File("root3")));
        byte[] rh = BranchDTO.joinRoot(c, new FileInputStream(new File("root4")));

        if (!Arrays.equals(rh, root))
            logError("OOPS! FRH: " + bytesToHex(rh) + "; expected: " + bytesToHex(root));
        log("TLC2: " + BranchDTO.totalLeafCount(c));
        log(("Weryfikacja root poprawna: " + BranchDTO.verifyRoot(c, null)));
    }


    static void mainC() throws Exception {
//        partialRoot();
//        testy1();
        Branch b = load("out.byte");
        log("Weryfikacja poprawna? " + b.verify());
        log("Loaded= Leaves: " + b.leaves.size() + " Roothash: " +
                bytesToHex(b.rootHash()) + " Branches: " + b.branches.size());
        save(b, "out2.byte");

        // NodeTests();
    }

    public static Branch load(String fname) throws FileNotFoundException {
        log("Restoring... " + fname);

        FileInputStream fis = new FileInputStream(new File(fname));
        return new Branch(fis);
    }

    public static void save(Branch branch, String fname) throws Exception {
        log("Saving...");
        FileOutputStream fos = new FileOutputStream(new File(fname));
        branch.serialize(fos);
        log("Saved:" + fname);
    }

    private static Branch randomBranch(int leaves, int n) throws Exception {

        Branch b = new Branch();
        Random r = new Random();
        for (int i = 0; i < leaves; i++) {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] buff = new byte[64];
            r.nextBytes(buff);
            byte[] h = messageDigest.digest(buff);
            byte[] ts = unsignedToBytes(rts());

            b.addLeaf(h, ts);
            Pair<byte[], byte[]> p = new Pair<>(h, ts);

            hasze.add(p);
        }

        if (jeden2) {
            jeden2 = false;
            hasze2.add(b.rootHash());
        }

        return b;
    }

    private static Branch randomBoB(int branches, int n) throws Exception {
        Branch b = new Branch();
        if (n > 0) {
            for (int i = 0; i < branches; i++) {
                b.addBranch(randomBoB(1, n - 1), unsignedToBytes(rts()));
            }
        } else {
            b.addBranch(randomBranch(100, 0), unsignedToBytes(rts()));
        }
        if (jeden1) {
            jeden1 = false;
            hasze2.add(b.rootHash());
        }
        return b;
    }

    static boolean compareFiles(String file1, String file2) throws IOException {
        File f1 = new File(file1);
        File f2 = new File(file2);
        boolean same = true;
        if (f1.length() == f2.length()) {
            int bufflen = 8192;
            int loops = (int) Math.ceil(((double) f1.length()) / bufflen);

            byte[] buff1 = new byte[bufflen];
            byte[] buff2 = new byte[bufflen];

            FileInputStream fs1 = new FileInputStream(f1);
            FileInputStream fs2 = new FileInputStream(f2);
            for (int i = 0; i < loops; i++) {
                fs1.read(buff1, 0, bufflen);
                fs2.read(buff2, 0, bufflen);
                if (!Arrays.equals(buff1, buff2)) {
                    same = false;
                    break;
                }
            }

        } else
            same = false;

        return same;
    }

    static void saveLoad() throws Exception {
        moar();
        save(root, "out.byte");
        Branch x = load("out.byte");
        save(x, "out2.byte");
        log("Files identical: " + compareFiles("out.byte", "out2.byte"));
    }

}


/*
     c = new Connection();
        c.connection.setAutoCommit(false);

//        root = deterministic();
//        moar();
//        saveHashes(hasze, "outHashes.txt");
//        jeden1 = true;
//        jeden2 = true;
////        moar();
////        searchFull();
//        searchCheck();
//        log("TOTAL LEAF COUNT: " + root.totalLeafCount());
//        log(root.verify());
//        long t = System.currentTimeMillis();
//        save(root, "out.byte");
//        Serialization.serializeTree(c, root);
//        log("Db serialization took: " + ((System.currentTimeMillis() - t) / 1000) + " seconds");
//        searchCheckDB();
//        t = System.currentTimeMillis();
//        searchFullDb();
//        log("search full db took: " + ((System.currentTimeMillis() - t) / 1000) + " seconds");
//        FromFileToSQL.serializeFromFile(c, new FileInputStream(new File("out.byte")));
//        Branch x = Deserialization.deserializeTree(c);
//        root = x;
//        save(root, "out.byte");
//        log("save db from file...");
//        log();
//        FromFileToSQL.serializeFromFile(c, new FileInputStream(new File("out.byte")));
//        log("\nDb Deserialization => File serialization...");
//        log();
//        Deserialization.deserializeTree(c, new FileOutputStream(new File("out2.byte")));
        logError("\nTOTAL LEAF COUNT: " + BranchDTO.totalLeafCount(c));
        logError("\nroot verify db: " + BranchDTO.verify(c));
//        save(x, "out2.byte");
        hasze = loadHashes("/Users/dawid/Desktop/kits/kitsC/kits/kits/hasze.txt");
        Collections.shuffle(hasze);
        searchFullDb();
 */

