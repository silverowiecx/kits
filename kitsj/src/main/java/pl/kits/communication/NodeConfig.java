package pl.kits.communication;


import pl.kits.classes.ByteUtils;
import pl.kits.classes.Sha256;

import java.util.LinkedList;
import java.util.List;

public class NodeConfig {
    /**
     * Node configuration
     */
    public Role role;                       // Node role
    public String IP;                       // External IP address of connection
    public int port;                        // External port of connection
    public byte[] hash;                     // ID hash
    public List<NodeConfig> higher;         // Known Nodes from higher level
    public List<NodeConfig> lower;          // Known Nodes from lower level
    public List<NodeConfig> side;           // Known Nodes on the same level


    /**
     * Default constructor
     */
    NodeConfig() {
        higher = new LinkedList<>();
        lower = new LinkedList<>();
        side = new LinkedList<>();
    }

    public NodeConfig(byte[] hash) {
        this();
        this.hash = hash;
    }

    public NodeConfig(String toHash) {
        this(Sha256.hash(toHash));
    }

    /**
     * Role of Node
     */
    public enum Role {
        Core,
        Mid,
        Low
    }

    @Override
    public String toString() {
        return IP + ":" + port + " ID: " + ByteUtils.bytesToHex(hash);
    }
}