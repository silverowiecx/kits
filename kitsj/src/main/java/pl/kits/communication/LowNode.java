package pl.kits.communication;

import pl.kits.classes.Branch;
import pl.kits.classes.ByteUtils;
import pl.kits.classes.Sha256;

import java.io.IOException;
import java.util.Scanner;

import static pl.kits.classes.Log.log;


public class LowNode extends MidNode {
    public LowNode(NodeConfig n) throws IOException {
        super(n);
        this.config.role = NodeConfig.Role.Low;
    }

    public LowNode(NodeConfig n, Branch b) throws IOException {
        super(n, b);
        this.config.role = NodeConfig.Role.Low;
    }

    public void addHash() throws Exception {
        log("Add text to be hashed: ");
        byte[] hash = Sha256.hash((new Scanner(System.in)).nextLine());
        hash = addHash(hash);
        log("Current root hash: " + ByteUtils.bytesToHex(hash));
    }
}
