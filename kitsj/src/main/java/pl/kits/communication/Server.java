package pl.kits.communication;

import static pl.kits.classes.ByteUtils.bytesToUnsigned;
import static pl.kits.classes.Log.*;

import pl.kits.Tests;
import pl.kits.classes.*;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Server extends Thread {

    private ServerSocket server;
    public boolean running;
    public Branch root;
    public ArrayList<Client> clients;
    public NodeConfig config;


    /**
     * Default constructor
     */
    private Server() {
        clients = new ArrayList<>();
        root = new Branch();
        config = new NodeConfig();
        running = false;
    }

    public Server(NodeConfig n) throws IOException {
        this();
        this.config = n;
        server = new ServerSocket(config.port);
    }


    public Server(NodeConfig n, Branch b) throws IOException {
        this(n);
        this.root = b;
    }


    /**
     * Send branch upwards
     *
     * @param branch Branch to be sent
     * @return Timestamp and root hash where Branch was saved
     */
    static byte[] sendUp(Branch branch) {
        return Commons.Cnt;
    }

    /**
     * Load Branch from stream
     *
     * @param stream stream to be read
     * @return Branch object
     */
    static Branch loadBranch(ByteArrayInputStream stream) {
        Branch b = new Branch(stream);
        if (b.verify())
            return b;
        else
            return new Branch();
    }

    /**
     * Send Branch with Leaf on which was received to side Node, return confirmation
     *
     * @param branch
     * @param leaf
     * @return if received correctly
     */
    static boolean sendSide(Branch branch, Leaf leaf) {
        return true;
        //?????
    }

    /**
     * Makes the server waiting for connections
     */
    void listen() {
        while (running) {
            try {
                server.setSoTimeout(1000);
                Socket tcpClient = server.accept();
                Client client = new Client(tcpClient, this);
                clients.add(client);
                clients.get(clients.size() - 1).start();
                log("New incoming connection.");
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * Connects to a server
     *
     * @param config config of server to be connected with
     * @throws IOException
     */
    public void connect(NodeConfig config) throws IOException {
        if (!connectionExists(config)) {
            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(config.IP, config.port), 5000);
                Client client = new Client(socket, this);
                clients.add(client);
                clients.get(clients.size() - 1).start();
                log("Connected successfully to: " + config);
            } catch (IOException e) {
                logError("Could not connect to: " + config);
            }
        }
    }

    /**
     * A method that keeps connected to all known Nodes, should be scheduled periodically
     * @throws IOException
     */
    public abstract void connectToAll() throws IOException;

    /**
     * Check whether Server is already connected to requested Client
     *
     * @param config config of connection to be established
     * @return if already connected
     */
    private boolean connectionExists(NodeConfig config) {
        return clients.stream().anyMatch(n -> Arrays.equals(n.connectedTo, config.hash));
    }

    @Override
    public void run() {
        try {
            boot();
            running = true;
            listen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Branch getBranch() {
        return root;
    }

    /**
     * Add hash that was given by the client
     *
     * @param hash hash to be added
     * @return 32 byte newly created root hash
     * @throws Exception when problem occurs (not known yet)
     */
    public byte[] addHash(byte[] hash) throws Exception {
        return root.addLeaf(hash, Timestamp.get());
    }

    /**
     * Send whole Root
     *
     * @return serialized Root
     * @throws Exception when problem occurs (not known yet)
     */
    public byte[] sendRoot() throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        root.serializeRoot(stream);
        return stream.toByteArray();
    }

    /**
     * Send Root from given hash
     *
     * @param hash hash where desired Root starts
     * @return serialized Root
     * @throws Exception when problem occurs (not known yet)
     */
    byte[] sendRoot(byte[] hash) throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        root.serializeRootFrom(hash, stream);
        return stream.toByteArray();
    }

    /**
     * Send Branch of given hash
     *
     * @param hash hash to be found
     * @return serialized Branch
     */
    byte[] sendBranch(byte[] hash) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        root.serializeBranch(hash, stream);
        return stream.toByteArray();
    }

    /**
     * Send current root hash
     *
     * @return root hash
     */
    byte[] sendCurrentRootHash() {
        return root.rootHash();
    }

    /**
     * Add Branch to Root
     *
     * @param branch to be added
     * @return current root hash
     * @throws Exception when problem occurs (not known yet)
     */
    byte[] addBranch(Branch branch) throws Exception {
        return root.addBranch(branch, Timestamp.get());
    }

    /**
     * Dump branch that was not sent (because of lack of connections)
     *
     * @param b branch to be dumped to file
     * @throws Exception
     */
    void saveUnsentBranch(Branch b) throws Exception {
        File tmpDir = new File("unsentRoot_" + bytesToUnsigned(Timestamp.get()));
        b.serialize(new FileOutputStream(tmpDir));
        log("Unsent root serialized successfully! Filename: " + tmpDir.getName());
    }

    /**
     * Check if unsent file is present, then return it
     *
     * @return unsent file or null
     */
    File loadUnsentBranchFile() {
        File f = new File(".");
        File[] matchingFiles = f.listFiles((dir, name) -> name.startsWith("unsentRoot_"));
        if (matchingFiles != null) {
            if (matchingFiles.length > 0) {
                Arrays.stream(matchingFiles).sorted();
                return matchingFiles[0];
            }
        }
        return null;
    }

    /**
     * Logic of server boot depending on the role
     */
    protected abstract void boot() throws Exception;

    /**
     * Close Server and all clients
     */
    public void close() throws Exception {
        running = false;
        for (Client client : clients)
            client.close();
        if (config.role == NodeConfig.Role.Core)
            Tests.save(root, "dump.byte");
    }

    /**
     * Send root to mid Node
     *
     * @throws Exception when failed
     */
    public abstract void sendRootUpwards() throws Exception;

    /**
     * Add hash by creating Leaf in root of Low Node
     * @throws Exception when failed
     */
    public abstract void addHash() throws Exception;
}