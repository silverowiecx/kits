package pl.kits.communication;

import pl.kits.classes.Branch;

import static pl.kits.classes.ByteUtils.*;
import static pl.kits.classes.Log.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;


public class Client extends Thread {
    /**
     * Client class that handles connections
     */

    private static final byte ok = 0;
    private static final byte rootPls = 1;
    private static final byte bye = 2;
    private static final byte rootPart = 3;
    private static final byte newLeaf = 4;
    private static final byte newBranch = 5;
    private static final byte branchPls = 6;
    private static final byte hashPls = 7;

    private DataInputStream reader;
    private DataOutputStream writer;

    byte[] connectedTo;
    private Server server;
    private Socket client;

    /**
     * Constructor that starts communication between connected clients
     *
     * @param client Client to be used
     */
    public Client(Socket client, Server server) throws IOException {
        this.client = client;
        this.server = server;
        writer = new DataOutputStream(client.getOutputStream());
        reader = new DataInputStream(client.getInputStream());
        switch (server.config.role) {
            case Mid:
            case Low:
            case Core:
                // TODO schedule tasks depending on role
                break;
        }
    }

    /**
     * Initialize connection
     */
    public void connect() {
        try {
            sendMessage(server.config.hash);

            byte[] hash = new byte[32];
            reader.read(hash);
            log("hash read: " + bytesToHex(hash));
            boolean ok = false;

            List<NodeConfig> configList = new LinkedList<>();
            configList.addAll(server.config.higher);
            configList.addAll(server.config.lower);
            configList.addAll(server.config.side);

            for (NodeConfig nc : configList)
                if (Arrays.equals(nc.hash, hash)) {
                    ok = true;
                    break;
                }
            if (ok) {
                if (server.clients.stream().filter(c -> server.config.higher
                        .stream()
                        .anyMatch(x -> Arrays.equals(x.hash, c.connectedTo) &&
                                (x.role == NodeConfig.Role.Mid || x.role == NodeConfig.Role.Core))).count() < 1) {
                    logError("Incoming connection, but no higher Node connected.");
                    close();
                } else {
                    connectedTo = hash;
                    log("Authenticated with: " + bytesToHex(connectedTo));
                    processCommands();
                }
            } else close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close connection
     */
    void close() {
        try {
            client.close();
            server.clients.remove(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Ports that can be opened
     */
    private static Stack<Integer> ports = new Stack<>();

    /**
     * Get the rest of Root, starting from given Hash
     *
     * @param branch Branch to which the rest should be appended
     * @param h   starting hash
     * @return complete Branch
     */
    public Branch getRootPart(final Branch branch, byte[] h) throws IOException {
        byte[] hash = new byte[32];
        System.arraycopy(h, 0, hash, 0, 32);

        sendMessage(rootPart);

        Socket tempSocket = getTempSocket();
        InputStream tempInput = tempSocket.getInputStream();
        OutputStream tempOutput = tempSocket.getOutputStream();

        byte[] resp = new byte[1];

        getMessage(resp, tempInput);
        if (resp[0] == ok) {
            sendMessage(switchEndianness(hash), tempOutput);
            byte[] blen = new byte[4];
            getMessage(blen, tempInput);
            long len = bytesToUnsigned(switchEndianness(blen));
            if (len > 0) {
                byte[] buff = new byte[(int) len];
                getMessage(buff, tempInput);
                sendMessage(bye, tempOutput);
                try {
                    branch.joinRoot(new ByteArrayInputStream(buff));
                } catch (Exception e) {
                    logError(e.getMessage());
                    tempSocket.close();
                    return null;
                }
            }
            tempSocket.close();
            return branch;
        }
        tempSocket.close();
        return null;
    }

    /**
     * Send Branch
     *
     * @param b Branch to be sent
     * @return hash received by client or byte[0] when failed
     */
    public byte[] sendBranch(Branch b) throws IOException {
        try {
            sendMessage(newBranch);

            Socket tempSocket = getTempSocket();
            InputStream tempInput = tempSocket.getInputStream();
            OutputStream tempOutput = tempSocket.getOutputStream();

            byte[] resp = new byte[1];

            getMessage(resp, tempInput);

            if (resp[0] == ok) {
                byte[] blen = unsignedToBytes(b.serializedSize());
                sendMessage(switchEndianness(blen), tempOutput);
                b.serialize(tempOutput);
                tempOutput.flush();
                blen = new byte[4];
                getMessage(blen, tempInput);
                long len = bytesToUnsigned(switchEndianness(blen));
                if (len < 32)
                    return new byte[0];
                byte[] hash = new byte[(int) len];
                getMessage(hash, tempInput);
                sendMessage(bye, tempOutput);
                tempSocket.close();
                return switchEndianness(hash);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * Get Branch of given hash
     *
     * @param h hash of desired Branch
     * @return newly created Branch
     */
    public Branch getBranch(byte[] h) throws IOException {
        try {
            byte[] hash = new byte[32];
            System.arraycopy(h, 0, hash, 0, 32);

            sendMessage(branchPls);

            Socket tempSocket = getTempSocket();
            InputStream tempInput = tempSocket.getInputStream();
            OutputStream tempOutput = tempSocket.getOutputStream();

            byte[] resp = new byte[1];
            getMessage(resp, tempInput);

            if (resp[0] == ok) {
                sendMessage(switchEndianness(hash), tempOutput);

                byte[] blen = new byte[4];
                getMessage(blen, tempInput);
                long len = bytesToUnsigned(switchEndianness(blen));
                byte[] buff = new byte[(int) len];
                getMessage(buff, tempInput);
                sendMessage(bye, tempOutput);
                return new Branch(new ByteArrayInputStream(buff));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Send new hash to be saved
     *
     * @param h hash to be saved
     * @return confirmation
     */
    public byte[] sendHash(byte[] h) throws IOException {
        try {
            byte[] hash = new byte[32];
            System.arraycopy(h, 0, hash, 0, 32);

            sendMessage(newLeaf);

            Socket tempSocket = getTempSocket();
            InputStream tempInput = tempSocket.getInputStream();
            OutputStream tempOutput = tempSocket.getOutputStream();

            byte[] resp = new byte[1];

            getMessage(resp, tempInput);

            if (resp[0] == ok) {
                sendMessage(switchEndianness(hash), tempOutput);

                byte[] response = new byte[32];
                getMessage(response, tempInput);
                sendMessage(bye, tempOutput);
                tempSocket.close();
                return response;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * Response for requests from connected Node
     */
    private void processCommands() {
        byte[] resp;
        byte[] hash;
        byte[] command;
        byte[] tempPort;
        byte[] buff;
        Socket tempSocket;
        InputStream tempInput;
        OutputStream tempOutput;
        while (!client.isClosed()) {
            try {
                command = new byte[1];
                getMessage(command);

                switch (command[0]) {
                    case rootPls:
                        log("RootPls received");

                        tempPort = new byte[4];
                        getMessage(tempPort);

                        tempSocket = getTempSocket(tempPort);
                        tempOutput = tempSocket.getOutputStream();
                        tempInput = tempSocket.getInputStream();

                        sendMessage(ok, tempOutput);

                        log("OK sent");
                        log("LENGTH TO SEND " + server.root.serializedRootSize());
                        byte[] blen = unsignedToBytes(server.root.serializedRootSize());

                        sendMessage(switchEndianness(blen), tempOutput);

                        log("wyslalem dlugosc");
                        server.root.serializeRoot(tempOutput);
                        tempOutput.flush();

                        log("wyslalem root");
                        resp = new byte[1];

                        getMessage(resp, tempInput);
                        if (resp[0] != bye)
                            throw new Exception("No confirmation");
                        tempSocket.close();
                        break;
                    case rootPart:
                        log("rootPart received");

                        tempPort = new byte[4];
                        getMessage(tempPort);
                        tempSocket = getTempSocket(tempPort);
                        tempOutput = tempSocket.getOutputStream();
                        tempInput = tempSocket.getInputStream();

                        sendMessage(ok, tempOutput);

                        hash = new byte[32];
                        tempInput.read(hash);

                        sendMessage(server.sendRoot(switchEndianness(hash)), tempOutput);

                        resp = new byte[4];
                        getMessage(resp, tempInput);

                        if (resp[0] != bye)
                            throw new Exception("No confirmation");
                        tempSocket.close();
                        break;
                    case newLeaf:
                        log("newLeaf received");

                        tempPort = new byte[4];
                        getMessage(tempPort);
                        tempSocket = getTempSocket(tempPort);
                        tempOutput = tempSocket.getOutputStream();
                        tempInput = tempSocket.getInputStream();

                        sendMessage(ok, tempOutput);
                        hash = new byte[32];
                        getMessage(hash, tempInput);
                        buff = server.addHash(switchEndianness(hash));
                        sendMessage(switchEndianness(buff), tempOutput);

                        resp = new byte[4];
                        getMessage(resp, tempInput);

                        if (resp[0] != bye)
                            throw new Exception("No confirmation");
                        tempSocket.close();
                        break;
                    case newBranch:
                        log("newBranch received");
                        tempPort = new byte[4];
                        getMessage(tempPort);

                        tempSocket = getTempSocket(tempPort);
                        tempOutput = tempSocket.getOutputStream();
                        tempInput = tempSocket.getInputStream();

                        sendMessage(ok, tempOutput);

                        blen = new byte[4];
                        getMessage(blen, tempInput);
                        long len = bytesToUnsigned(switchEndianness(blen));

                        Branch b = new Branch(tempInput);
                        hash = server.addBranch(b);
                        sendMessage(unsignedToBytes(hash.length), tempOutput);

                        sendMessage(hash, tempOutput);

                        resp = new byte[1];
                        getMessage(resp, tempInput);
                        if (resp[0] != bye)
                            throw new Exception("No confirmation");
                        tempSocket.close();
                        break;
                    case branchPls:
                        log("branchPls received");
                        tempPort = new byte[4];
                        getMessage(tempPort);

                        tempSocket = getTempSocket(tempPort);
                        tempOutput = tempSocket.getOutputStream();
                        tempInput = tempSocket.getInputStream();

                        sendMessage(ok, tempOutput);
                        hash = new byte[32];
                        getMessage(hash, tempInput);
                        buff = server.sendBranch(switchEndianness(hash));
                        sendMessage(unsignedToBytes(buff.length), tempOutput);

                        sendMessage(buff, tempOutput);

                        resp = new byte[1];
                        getMessage(resp, tempInput);
                        if (resp[0] != bye)
                            throw new Exception("No confirmation");
                        tempSocket.close();
                        break;
                    case hashPls:
                        log("hashPls received");
                        tempPort = new byte[4];
                        getMessage(tempPort);

                        tempSocket = getTempSocket(tempPort);
                        tempOutput = tempSocket.getOutputStream();
                        tempInput = tempSocket.getInputStream();

                        sendMessage(ok, tempOutput);
                        hash = new byte[32];
                        getMessage(hash, tempInput);
                        buff = server.root.find(switchEndianness(hash));
                        sendMessage(buff, tempOutput);

                        resp = new byte[1];
                        getMessage(resp, tempInput);
                        if (resp[0] != bye)
                            throw new Exception("No confirmation");
                        tempSocket.close();
                        break;
                    default:
                        logError("WTF I JUST READ: " + Arrays.toString(command) + "\nhex: " + bytesToHex(command));
                        logError("Closing this client...");
                        while (!this.isInterrupted())
                            this.interrupt();
                        close();
                        return;
                }
            } catch (SocketException ignored) {
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Start the Client thread
     */
    @Override
    public void run() {
        connect();
    }

    /**
     * Send a message through main OutputStream
     *
     * @param msg message to be sent
     * @throws IOException
     */
    private void sendMessage(byte[] msg) throws IOException {
        sendMessage(msg, writer);
    }

    /**
     * Send a message through given OutputStream
     * @param msg message to be sent
     * @param out OutputStream to be used
     * @throws IOException
     */
    private void sendMessage(byte[] msg, OutputStream out) throws IOException {
        out.write(msg);
        out.flush();
    }

    /**
     * Send a message through main OutputStream
     * @param msg message to be sent
     * @throws IOException
     */
    private void sendMessage(byte msg) throws IOException {
        sendMessage(msg, writer);
    }

    /**
     * Send a message through given OutputStream
     * @param msg message to be sent
     * @param out OutputStream to be used
     * @throws IOException
     */
    private void sendMessage(byte msg, OutputStream out) throws IOException {
        out.write(msg);
        out.flush();
    }

    /**
     * Receive a message through main InputStream
     * @param buff buffer to be written
     * @throws IOException
     */
    private void getMessage(byte[] buff) throws IOException {
        getMessage(buff, reader);
    }

    /**
     * Receive a message through given InputStream
     * @param buff buffer to be written
     * @throws IOException
     */
    private void getMessage(byte[] buff, InputStream in) throws IOException {
        in.read(buff);
    }

    static {
        for (int i = 9010; i < 10000; i++) {
            ports.push(i);
        }
    }

    /**
     * Creates temporary socket to send response
     * @return Socket with timeout set
     * @throws IOException
     */
    private Socket getTempSocket(byte[] tempPort) throws IOException {
        Socket tempSocket = new Socket(client.getInetAddress().getHostAddress(), (int) bytesToUnsigned(switchEndianness(tempPort)));
        tempSocket.setSoTimeout(10000);
        return tempSocket;
    }

    private static int getPort() {
        return ports.pop();
    }

    /**
     * Read the whole Root
     *
     * @return Fulfilled Branch or empty Branch when failed
     */
    public Branch getRoot() throws InterruptedException {
        try {
            sendMessage(rootPls);

            Socket tempSocket = getTempSocket();
            InputStream tempInput = tempSocket.getInputStream();
            OutputStream tempOutput = tempSocket.getOutputStream();

            byte[] resp = new byte[1];
            getMessage(resp, tempInput);

            if (resp[0] == ok) {
                logError("dostalem okej");
                byte[] blen = new byte[4];
                getMessage(blen, tempInput);
                logError("odczytalem dlugosc");

                int len = (int) bytesToUnsigned(switchEndianness(blen));
                log("LENGTH " + len);

                Branch b = Branch.onlyRoot(tempInput);

                sendMessage(bye, tempOutput);

                tempSocket.close();
                return b;
            }
            logError("NIE DOSTALEM OKEJ :(");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Asks Core Node if it has a hash
     *
     * @param hash hash to be found
     * @return timestamp of stored hash
     */
    public byte[] askForHash(byte[] hash) throws IOException {
        sendMessage(hashPls);

        Socket tempSocket = getTempSocket();
        InputStream tempInput = tempSocket.getInputStream();
        OutputStream tempOutput = tempSocket.getOutputStream();

        byte[] resp = new byte[1];

        getMessage(resp, tempInput);

        if (resp[0] == ok) {
            sendMessage(switchEndianness(hash), tempOutput);
            byte[] ts = new byte[4];
            getMessage(ts, tempInput);
            sendMessage(bye, tempOutput);
            ts = switchEndianness(ts);
            tempSocket.close();
            return ts;
        }
        tempSocket.close();
        return new byte[0];
    }


    /**
     * Creates temporary server to get response
     *
     * @return ServerSocket with timeout set
     * @throws IOException
     */
    private ServerSocket getTempServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(getPort());
        serverSocket.setSoTimeout(20000);
        return serverSocket;
    }

    /**
     * Creates temporary socket from accepted connection
     * @return Socket to be used in temporary communication
     * @throws IOException
     */
    private Socket getTempSocket() throws IOException {
        ServerSocket tempServ = getTempServer();
        byte[] tempPort = unsignedToBytes(tempServ.getLocalPort());
        sendMessage(switchEndianness(tempPort));
        Socket s = tempServ.accept();
        ports.push(tempServ.getLocalPort());
        tempServ.close();
        return s;
    }

}
