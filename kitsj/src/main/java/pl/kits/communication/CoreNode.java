package pl.kits.communication;

import pl.kits.Tests;
import pl.kits.classes.Branch;
import pl.kits.database.BranchDTO;
import pl.kits.database.Connection;

import java.io.IOException;
import java.io.InvalidClassException;

import static pl.kits.classes.Log.log;


public class CoreNode extends Server {
    public CoreNode(NodeConfig n) throws IOException {
        super(n);
        this.config.role = NodeConfig.Role.Core;
    }

    public CoreNode(NodeConfig n, Branch b) throws IOException {
        super(n, b);
        this.config.role = NodeConfig.Role.Core;
    }

    @Override
    public void connectToAll() throws IOException {
        for (NodeConfig conf : config.side)
            connect(conf);
    }

    @Override
    public void boot() throws Exception {
        // log("DB verify: "+BranchDTO.verify(new Connection()));
        root = Tests.load("dump.byte");
        log("root verify: " + root.verify());
        // TODO
    }

    @Override
    public void addHash() throws Exception {
        throw new InvalidClassException("Only Low Nodes can do that!");
    }

    @Override
    public void sendRootUpwards() throws Exception {
        throw new InvalidClassException("Core Node cannot do that!");
    }
}
