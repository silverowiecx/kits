package pl.kits.communication;

import pl.kits.classes.Branch;
import pl.kits.classes.Leaf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static pl.kits.classes.Log.log;
import static pl.kits.classes.Log.logError;

public class MidNode extends Server {

    public MidNode(NodeConfig n) throws IOException {
        super(n);
        this.config.role = NodeConfig.Role.Mid;
    }

    public MidNode(NodeConfig n, Branch b) throws IOException {
        super(n, b);
        this.config.role = NodeConfig.Role.Mid;
    }

    @Override
    public void connectToAll() throws IOException {
        for (NodeConfig conf : config.higher)
            connect(conf);
    }

    @Override
    protected void boot() throws Exception {
        // CHYBA POWINNO BYC WYMIENIONE NA TIMERTASK? https://dzone.com/articles/schedulers-in-java-and-spring
        connectToAll();
        sendRootUpwards();
        Thread t = new Thread(() -> {
            long time = System.currentTimeMillis();
            while (!this.isInterrupted()) {
                try {
                    sleep(1000);
                    if (root.numberOfRootLeaves() > 99) {
                        sendRootUpwards();
                        time = System.currentTimeMillis();
                    } else if (System.currentTimeMillis() / 1000 - time > 600000 && root.numberOfRootLeaves() > 0) {
                        sendRootUpwards();
                        time = System.currentTimeMillis();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    /**
     * Method that is executed by Thread when lack of connections
     */
    private Runnable connectToAllRunnable = () -> {
        try {
            connectToAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    /**
     * Send root upwards to core or mid depending on role
     *
     * @throws Exception when failed
     */
    public void sendRootUpwards() throws Exception {
        Predicate<Client> predicate = config.role == NodeConfig.Role.Mid ? client -> config.higher
                .stream()
                .anyMatch(x -> Arrays.equals(x.hash, client.connectedTo) &&
                        (x.role == NodeConfig.Role.Mid || x.role == NodeConfig.Role.Core))
                : client -> config.higher
                .stream()
                .anyMatch(x -> Arrays.equals(x.hash, client.connectedTo) &&
                        (x.role == NodeConfig.Role.Mid));

        List<Client> toSend = clients
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());

        if (toSend.size() < 1) {
            logError("There is no Node available to send!");
            if (root.numberOfRootLeaves() > 0)
                saveUnsentBranch(root);
            root = new Branch();
            return;
        }
        File awaitedFile = loadUnsentBranchFile();
        while (awaitedFile != null) {
            boolean sent = false;
            Branch awaited = new Branch(new FileInputStream(awaitedFile));
            try {
                if (askForHash(awaited.leaves.get(0))
                        && askForHash(awaited.leaves.get(awaited.numberOfRootLeaves() - 1))) {
                    awaitedFile.delete();
                    awaitedFile = loadUnsentBranchFile();
                    continue;
                }
            } catch (IOException e) {
                logError(e.getMessage());
                return;
            }
            for (Client client : toSend) {
                try {
                    byte[] receivedRH = client.sendBranch(awaited);
                    if (Arrays.equals(receivedRH, awaited.rootHash())) {
                        sent = true;
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!sent) {
                logError("Sending awaited root failed! Retrying in a moment...");
                Thread retryConnection = new Thread(connectToAllRunnable);
                retryConnection.start();
                Thread.sleep(1000);
            } else {
                log("Awaited Root (ts: " + awaitedFile.getName().substring(11, 21) + ") sent!");
                awaitedFile.delete();
                awaitedFile = loadUnsentBranchFile();
            }
        }
        boolean sent = false;
        if (root.numberOfRootLeaves() < 1) {
            return;
        }
        for (Client client : toSend) {
            try {
                byte[] receivedRH = client.sendBranch(root);
                if (Arrays.equals(receivedRH, root.rootHash())) {
                    sent = true;
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!sent) {
            logError("Sending root failed! Storing on disk...");
//                JOptionPane.showMessageDialog(null, "Sending root failed! Storing on disk..."
//                , "Error while sending root!", JOptionPane.ERROR_MESSAGE);
            saveUnsentBranch(root);
            root = new Branch();
            Thread retryConnection = new Thread(connectToAllRunnable);
            retryConnection.start();
        } else {
            log("Root sent!");
            // TODO clear root (switch everything to db)
            root = new Branch();  // Main.clearDb(new Connection());
        }
    }


    @Override
    public void addHash() throws Exception {
        throw new InvalidClassException("Only Low Nodes can do that!");
    }

    /**
     * Check whether Core Node has a hash
     *
     * @param l Leaf of hash to be asked for
     * @return true if hash is already stored in Core Nodes
     */
    boolean askForHash(Leaf l) throws IOException {
        List<Client> toAsk = clients
                .stream()
                .filter(n -> config.higher
                        .stream()
                        .anyMatch(x -> Arrays.equals(x.hash, n.connectedTo) && x.role == NodeConfig.Role.Core))
                .collect(Collectors.toList());
        if (toAsk.size() < 1) {
            Thread retryConnection = new Thread(connectToAllRunnable);
            retryConnection.start();
            throw new IOException("No core node connected!");
        }
        for (Client client : toAsk) {
            if (Arrays.equals(client.askForHash(l.hash), l.timestamp))
                return true;
        }
        return false;
    }
}
