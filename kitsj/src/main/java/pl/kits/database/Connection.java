package pl.kits.database;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Savepoint;

public class Connection {
    /**
     * Class for creation a connection object
     * fields should be replaced with methods that should be initialized from outside the application
     */
    private String driverName = "com.mysql.cj.jdbc.Driver";
    public java.sql.Connection connection;
    private String user = "root";
    private String password = "Mnta!1234";
    private String url = "jdbc:mysql://localhost:3306/KITS?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private Savepoint save;

    /**
     * Gets connection, calls savepoint creation
     */
    public Connection() {
        connection = getConnection();
        beginTransaction();
    }

    /**
     * Gets connection for given driver and database connection properties
     *
     * @return connection object
     */
    private java.sql.Connection getConnection() {
        try {
            Class.forName(driverName);
            java.sql.Connection c = DriverManager.getConnection(url, user, password);
            c.setAutoCommit(false);
            return c;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Savepoint creation
     */
    public void beginTransaction() {
        try {
            save = connection.setSavepoint();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Applying changes
     */
    void commitTransaction() {
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restoring database from savepoint
     */
    void rollbackTransaction() {
        try {
            connection.rollback(save);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
