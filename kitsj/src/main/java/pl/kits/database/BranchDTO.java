package pl.kits.database;

import pl.kits.classes.*;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;

import static pl.kits.classes.ByteUtils.bytesToUnsigned;
import static pl.kits.classes.ByteUtils.switchEndianness;
import static pl.kits.classes.Log.log;


public class BranchDTO {
    /**
     * Simply counts number of leaves in database
     *
     * @param c opened connection
     * @return leaf count
     */
    static public int totalLeafCount(Connection c) {
        try {
            PreparedStatement statement = c.connection.prepareStatement("CALL TOTAL_LEAF_COUNT();");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static byte[] addLeaf(Connection c, Leaf l) throws SQLException {
        addLeaf(c, l, null);
        return getRootHash(c);
    }

    /**
     * Adds Leaf or Branch under the given parent
     *
     * @param c          opened connection
     * @param l          Leaf of any type to be added
     * @param parentHash Hash of Branch where the given Leaf should be added
     * @return new root hash of branch where leaf was added
     */
    private static byte[] addLeaf(Connection c, Leaf l, byte[] parentHash) throws SQLException {
        saveLeaf(c, l, parentHash);
        c.commitTransaction();
        return getRootHash(c, parentHash);
    }

    /**
     * Add fresh leaf by hash and timestamp under given parent
     *
     * @param c          opened database connection
     * @param hash       hash from which Leaf should be created
     * @param timestamp  timestamp of Leaf to be created
     * @param parentHash hash of the parent Leaf
     * @return new rootHash byte[32]
     */
    static public byte[] addLeaf(Connection c, byte[] hash, byte[] timestamp, byte[] parentHash) throws Exception {
        if (hash.length != 32 || timestamp.length != 4)
            throw new Exception("Bad data size for adding Leaf!");
        Leaf l = new Leaf(hash, timestamp);
        l.rootHash = Sha256.hash(getRootHash(c, parentHash), l.hash, l.timestamp);
        return addLeaf(c, l, parentHash);
    }

    /**
     * Gets rootHash of given hash of Branch
     *
     * @param c          opened database connection
     * @param parentHash hash of Branch
     * @return rootHash of given hash of Branch
     */
    private static byte[] getRootHash(Connection c, byte[] parentHash) throws SQLException {
        if (parentHash == null) return getRootHash(c);
        try {
            PreparedStatement statement = c.connection.prepareStatement("CALL ROOTHASH(?);");
            statement.setBytes(1, parentHash);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getBytes("roothash");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new byte[32];
    }

    /**
     * Finds a timestamp of given hash
     *
     * @param c    opened connection
     * @param hash hash which timestamp should be found
     * @return found timestamp or new byte[4]
     */
    static public byte[] find(Connection c, byte[] hash) {
        try {
            PreparedStatement statement = c.connection.prepareStatement("CALL FIND(?)");
            statement.setBytes(1, hash);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getBytes("timestamp");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new byte[4];
    }

    /**
     * Exact stuff to proofFind from Branch class. Returned Branch should be verified once again
     *
     * @param c      opened connection
     * @param child  child branch, should be null for invocation
     * @param parent id of database parent, should be null for invocation
     * @param hash   hash to be found in db
     * @return Tree path to desired hash
     * @throws Exception when connection error or leaf creation
     */
    private static Branch proofFind(Connection c, Branch child, Integer parent, byte[] hash) throws Exception {
        if (child == null) {
            PreparedStatement statement = c.connection.prepareStatement("CALL PROOF_DEEP_BRANCH(?);");
            statement.setBytes(1, hash);
            ResultSet resultSet = statement.executeQuery();
            Branch temp = new Branch();
            while (resultSet.next()) {
                Leaf l = new Leaf(resultSet.getBytes("hash"), resultSet.getBytes("timestamp"));
                l.rootHash = resultSet.getBytes("roothash");
                temp.leaves.add(l);
            }
            resultSet.previous();
            return proofFind(c, temp, resultSet.getInt("parent"), hash);
        } else {
            if (parent == null)
                return child;
            Branch temp = new Branch();
            PreparedStatement statement = c.connection.prepareStatement("CALL PROOF_UPPER_PARENT(?);");
            statement.setInt(1, parent);
            ResultSet resultSet = statement.executeQuery();
            Integer upper_parent;
            if (resultSet.next())
                upper_parent = resultSet.getInt("parent");
            else
                upper_parent = null;
            if (upper_parent == null)
                statement = c.connection.prepareStatement("CALL GET_ROOT_LEAVES();");
            else {
                statement = c.connection.prepareStatement("CALL GET_LEAVES_BY_ID(?);");
                statement.setInt(1, upper_parent);
            }
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Leaf l = new Leaf(resultSet.getBytes("hash"), resultSet.getBytes("timestamp"));
                l.rootHash = resultSet.getBytes("roothash");
                if (resultSet.getInt("id_leaf") == parent)
                    l.type = Commons.Branch;
                temp.leaves.add(l);
            }
            temp.branches.add(child);
            if (upper_parent == null)
                return temp;
            else
                return proofFind(c, temp, upper_parent, hash);
        }
    }

    /**
     * Recursive down-up rebuild of path to desired hash. Branch should be verified afterwards.
     *
     * @param c    opened connection
     * @param hash hash to be found
     * @return path to hash
     * @throws Exception when failed
     */
    public static Branch proofFind(Connection c, byte[] hash) throws Exception {
        return proofFind(c, null, null, hash);
    }


    /**
     * Verifies tree in database
     *
     * @param c opened connection
     * @return result of verification
     * @throws Exception when failed
     */
    public static boolean verify(Connection c) throws Exception {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_ROOT_LEAVES();");
        return verify(c, statement);
    }

    private static boolean verify(Connection c, PreparedStatement statement) throws Exception {
        Branch branch = new Branch();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            byte[] ts = resultSet.getBytes("timestamp");
            byte[] hash = resultSet.getBytes("hash");
            byte[] rootHash = resultSet.getBytes("roothash");
            byte[] type = resultSet.getBytes("type");
            Leaf l = new Leaf(hash, ts);
            l.type = type;
            l.rootHash = rootHash;
            branch.leaves.add(l);
        }
        if (!branch.verifyRoot())
            return false;
        for (Leaf l : branch.leaves)
            if (Arrays.equals(l.type, Commons.Branch)) {
                if (!verifyRoot(c, l.hash)) return false;
            }
        return true;
    }


    /**
     * Verifies particular Branch in database
     *
     * @param c          opened connection
     * @param parentHash hash of Branch to be verified
     * @return result of verification
     * @throws Exception when failed
     */
    public static boolean verifyRoot(Connection c, byte[] parentHash) throws Exception {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_LEAVES(?)");
        statement.setBytes(1, parentHash);
        return verify(c, statement);
    }

    /**
     * Gets rootHash of main Branch
     *
     * @param c opened database connection
     * @return rootHash of given hash of Branch
     */
    public static byte[] getRootHash(Connection c) throws SQLException {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_ROOTHASH();");
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next())
            return resultSet.getBytes("roothash");
        return new byte[32];
    }


    /**
     * Add Branch to the database
     *
     * @param c connection to be used
     * @param b branch to be added
     * @return rootHash
     * @throws SQLException
     */
    public static byte[] addBranch(Connection c, Branch b) throws Exception {
        byte[] currentRh = getRootHash(c);
        Leaf leaf = new Leaf(b, Timestamp.get(), currentRh);
        byte[] rh = addLeaf(c, leaf);
        addBranch(c, b, rh);
        return rh;
    }


    /**
     * Adds Branch under desired parent
     *
     * @param c      connection to be used
     * @param branch branch to be added
     * @param parent parent under which Branch should be added
     * @throws Exception
     */
    private static void addBranch(Connection c, Branch branch, byte[] parent) throws Exception {
        Leaf testLeaf = getLeaf(c, branch.rootHash());

        assert testLeaf != null;
        if (Arrays.equals(testLeaf.hash, parent)) //czy jest zapisany liść na którym wisi branch
        {
            c.beginTransaction();
            onlyRoot(c, branch, parent);
            c.commitTransaction();
            for (int bc = 0; bc < branch.branches.size(); bc++) {
                addBranch(c, branch.branches.get(bc), branch.branches.get(bc).rootHash());
            }
        } else log("AB: parent not found");
    }

    /**
     * Retrieves Leaf by hash
     *
     * @param c    connection to be used
     * @param hash hash of the Leaf to be found
     * @return Retrieved Leaf
     * @throws Exception
     */
    private static Leaf getLeaf(Connection c, byte[] hash) throws Exception {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_LEAF(?);");
        statement.setBytes(1, hash);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            byte[] type = set.getBytes("type");
            byte[] h = set.getBytes("hash");
            byte[] roothash = set.getBytes("roothash");
            byte[] timestamp = set.getBytes("timestamp");
            Leaf l = new Leaf(h, timestamp);
            l.type = type;
            l.rootHash = roothash;
            return l;
        }
        return null;
    }


    /**
     * Adds only root Leaves without recursion
     *
     * @param c      connection to be used
     * @param branch brach to be added
     * @param parent hash of parent to which branch belongs
     */
    private static void onlyRoot(Connection c, Branch branch, byte[] parent) throws SQLException {
        for (int lc = 0; lc < branch.leaves.size(); lc++) {
            addLeaf(c, branch.leaves.get(lc), parent);
        }
    }

    /**
     * Sends root of given hash to the end
     *
     * @param c      connection to be userd
     * @param from   beginning hash
     * @param stream stream for output
     */
    public static void serializeRootFrom(Connection c, OutputStream stream, byte[] from) throws Exception {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_ROOT_LEAVES_FROM(?);");
        statement.setBytes(1, from);
        ResultSet set = statement.executeQuery();
        Branch b = new Branch();
        while (set.next()) {
            byte[] h = set.getBytes("hash");
            byte[] rh = set.getBytes("roothash");
            byte[] ts = set.getBytes("timestamp");
            byte[] type = set.getBytes("type");
            Leaf l = new Leaf(h, ts);
            l.rootHash = rh;
            l.type = type;
            b.leaves.add(l);
        }
        b.serializeRoot(stream);
    }

    /**
     * Joins root into Branch from input stream. Each new Leaf
     * is verified by hash and if it fails, it stops and returns current
     * roothash.
     *
     * @param c      connection to be used
     * @param stream input stream
     * @return extended Branch roothash or byte[0] when failed
     */
    public static byte[] joinRoot(Connection c, InputStream stream) throws Exception {
        c.beginTransaction();
        byte[] rh = getRootHash(c);
        byte[] id = new byte[1];
        stream.read(id);
        if (Arrays.equals(id, Commons.Root)) {
            byte[] bnum = new byte[4];
            stream.read(bnum);
            long num = bytesToUnsigned(switchEndianness(bnum));
            for (int i = 0; i < num; i++) {
                byte[] bleaf = new byte[69];
                stream.read(bleaf);
                Leaf l = new Leaf(bleaf);
                byte[] hash = Sha256.hash(rh, l.hash, l.timestamp);
                if (Arrays.equals(hash, l.rootHash)) {
                    saveLeaf(c, l, null);
                    rh = getRootHash(c);
                } else {
                    c.rollbackTransaction();
                    return new byte[0];
                }
            }
            c.connection.commit();
            return getRootHash(c);
        }
        return new byte[0];
    }

    // region serialization

    /**
     * Loads the whole tree from database and writes it into stream
     *
     * @param c      opened database connection
     * @param stream opened stream
     */
    public static void serializeTree(Connection c, OutputStream stream) {
        try {
            PreparedStatement statement = c.connection.prepareStatement("CALL GET_ROOT_LEAVES();");
            serialize(c, stream, statement);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads particular Branch from database and writes it into stream
     *
     * @param c          opened connection
     * @param stream     opened stream
     * @param parentHash hash of given branch
     * @throws Exception when fails
     */
    private static void serializeBranch(Connection c, OutputStream stream, byte[] parentHash) throws Exception {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_LEAVES(?)");
        statement.setBytes(1, parentHash);
        serialize(c, stream, statement);
    }

    /**
     * Loads Root leaves from database and writes them into stream
     *
     * @param c      connection to be used
     * @param stream stream to be used
     * @throws Exception
     */
    public static void serializeRoot(Connection c, OutputStream stream) throws Exception {
        PreparedStatement statement = c.connection.prepareStatement("CALL GET_ROOT_LEAVES();");
        Branch branch = new Branch();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            byte[] ts = resultSet.getBytes("timestamp");
            byte[] hash = resultSet.getBytes("hash");
            byte[] rootHash = resultSet.getBytes("roothash");
            byte[] type = resultSet.getBytes("type");
            Leaf l = new Leaf(hash, ts);
            l.type = type;
            l.rootHash = rootHash;
            branch.leaves.add(l);
        }
        if (!branch.verifyRoot())
            throw new Exception("Particular Branch verify failed!");
        branch.serializeRoot(stream);
    }

    /**
     * Verifies particular Branch and invokes serialization into stream
     *
     * @param c         opened database connection
     * @param stream    opened stream
     * @param statement database procedure invocation statement
     * @throws Exception when fails
     */
    private static void serialize(Connection c, OutputStream stream, PreparedStatement statement) throws Exception {
        Branch branch = new Branch();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            byte[] ts = resultSet.getBytes("timestamp");
            byte[] hash = resultSet.getBytes("hash");
            byte[] rootHash = resultSet.getBytes("roothash");
            byte[] type = resultSet.getBytes("type");
            Leaf l = new Leaf(hash, ts);
            l.type = type;
            l.rootHash = rootHash;
            branch.leaves.add(l);
        }
        if (!branch.verifyRoot())
            throw new Exception("Particular Branch verify failed!");
        branch.serializeRoot(stream);
        for (Leaf l : branch.leaves)
            if (Arrays.equals(l.type, Commons.Branch)) {
                serializeBranch(c, stream, l.hash);
            }
    }

    // endregion

    // region deserialization
    private static void deserializeTree(Connection c, InputStream stream, byte[] parent) throws Exception {
        byte[] start = new byte[1];
        stream.read(start);
        if (!Arrays.equals(start, Commons.Root))
            throw new Exception("Root expected in data stream!");
        byte[] blen = new byte[4];
        stream.read(blen);
        switchEndianness(blen);
        long len = bytesToUnsigned(blen);
        ArrayList<byte[]> branchHashes = new ArrayList<>();
        Branch temp = new Branch();
        for (long i = 0; i < len; i++) {
            byte[] bleaf = new byte[69];
            stream.read(bleaf);
            byte[] type = new byte[1];
            System.arraycopy(bleaf, 0, type, 0, 1);
            if (Arrays.equals(type, Commons.Branch)) {
                branchHashes.add(Arrays.copyOfRange(bleaf, 1, 33));
            }
            temp.leaves.add(new Leaf(bleaf));
        }
        if (!temp.verifyRoot()) {
            throw new Exception("Particular Branch verify failed!");
        }
        for (Leaf l : temp.leaves)
            saveLeaf(c, l, parent);
        for (byte[] b : branchHashes) {
            c.commitTransaction();
            deserializeTree(c, stream, b);
        }
    }

    /**
     * Reads file from stream and inserts tree into db
     *
     * @param c      opened connection
     * @param stream stream with tree to be inserted
     * @throws Exception
     */
    public static void deserializeTree(Connection c, InputStream stream) throws Exception {
        deserializeTree(c, stream, null);
        c.commitTransaction();
    }


    /**
     * Inserts Leaf in database
     *
     * @param c          opened connection
     * @param l          leaf to be inserted
     * @param parentHash hash of Branch where Leaf should be placed
     * @return index where leaf was put or 0 when failed
     */
    private static int saveLeaf(Connection c, Leaf l, byte[] parentHash) {
        int result;
        String sqlAddLeafCommand = "CALL INSERT_LEAF(?, ?, ?, ?, ?);";
        try {
            PreparedStatement preparedAddLeafStatement = c.connection.prepareStatement(sqlAddLeafCommand);
            preparedAddLeafStatement.setBytes(1, l.type);
            preparedAddLeafStatement.setBytes(2, l.rootHash);
            preparedAddLeafStatement.setBytes(3, l.hash);
            preparedAddLeafStatement.setBytes(4, l.timestamp);
            if (parentHash == null)
                preparedAddLeafStatement.setNull(5, Types.BLOB);
            else
                preparedAddLeafStatement.setBytes(5, parentHash); // or reversed array
            result = preparedAddLeafStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            c.rollbackTransaction();
            return 0;
        }
        return result;
    }

    /**
     * Saves the whole tree in database
     *
     * @param c    opened connection
     * @param root root of the tree to be saved
     */
    public static void saveTreeObject(Connection c, Branch root) {
        for (Leaf l : root.leaves) {
            saveLeaf(c, l, null);
        }
        for (Branch b : root.branches)
            saveBranchObject(c, b);
    }

    /**
     * Saves the tree under given branch in database
     *
     * @param c      opened connection
     * @param branch Branch where the given tree starts
     */
    private static void saveBranchObject(Connection c, Branch branch) {
        for (Leaf l : branch.leaves) {
            saveLeaf(c, l, branch.rootHash());
        }
        for (Branch b : branch.branches) {
            saveBranchObject(c, b);
            c.commitTransaction();
        }
    }
    // endregion

    /**
     * Gets number of root leaves required for determining sending tree upwards
     *
     * @param c opened connection with database
     * @return number of root leaves placed in database
     * @throws SQLException when connection problem
     */
    public static int numberOfRootLeaves(Connection c) throws SQLException {
        PreparedStatement ps = c.connection.prepareStatement("SELECT COUNT(*) FROM LEAF WHERE PARENT IS NULL;");
        ResultSet rs = ps.executeQuery();
        if (rs.next())
            return rs.getInt(1);
        else return 0;
    }

    /**
     * Deletes all leaves in database -> new Branch()
     *
     * @param c opened connection with database
     * @throws SQLException when failed
     */
    public static void clear(Connection c) throws SQLException {
        PreparedStatement ps = c.connection.prepareStatement("DELETE FROM LEAF WHERE id_leaf>0;");
        ps.execute();
        c.connection.commit();
        c.beginTransaction();
    }
}