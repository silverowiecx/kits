package pl.kits;

import pl.kits.classes.Sha256;
import pl.kits.communication.*;

import java.util.Scanner;
import static pl.kits.classes.Log.log;
import static pl.kits.classes.Log.logError;

class UI {

    private Scanner sc;

    UI() {
        sc = new Scanner(System.in);
    }

    private static NodeConfig config(String toHash, String IP, int port, NodeConfig.Role role) {
        NodeConfig n = new NodeConfig(Sha256.hash(toHash));
        n.role = role;
        n.port = port;
        n.IP = IP;
        return n;
    }


    private NodeConfig getConf(boolean local) {
        String IP;
        if (local) {
            IP = "127.0.0.1";
        } else {
            log("Provide Node's IP:");
            IP = sc.nextLine();
        }
        log("Provide a listening port: ");
        int port = Integer.parseInt(sc.nextLine());
        log("Provide string to generate Hash ID: ");
        String id = sc.nextLine();
        log("Provide a Role: 0=Core, 1=Mid, 2=Low: ");
        try {
            NodeConfig.Role rola = NodeConfig.Role.values()[Integer.parseInt(sc.nextLine())];
            return config(id, IP, port, rola);
        } catch (ArrayIndexOutOfBoundsException e) {
            logError("Role chosen incorrectly, Config not saved. Try again:");
            return getConf(local);
        }
    }


    void InteractiveNode() throws Exception {
        log("Starting interactive node...");

        Server s;

        NodeConfig config = getConf(true);
        if (config.role == NodeConfig.Role.Low) {
            s = new LowNode(config);
        } else if (config.role == NodeConfig.Role.Mid) {
            s = new MidNode(config);
        } else {
            s = new CoreNode(config);
        }

        s.start();
        log("Server is running, what's next?");

        boolean koniec = false;
        while (!koniec) {
            log("Choose command: \n" +
                    "0 - Quit \n" +
                    "1 - Add Node Configuration to connect to \n" +
                    "2 - Connect to all known Nodes \n" +
                    "3 - Add hash to be stored \n" +
                    "4 - Send Tree upwards \n");
            int option = Integer.parseInt(sc.nextLine());

            switch (option) {
                case 0: // koniec
                    koniec = true;
                    s.running = false;
                    log("Closing...");
                    break;
                case 1: // dodaj konfig
                    NodeConfig conf = getConf(false);
                    log("Where to add? 1=Lower, 2=Side, 3=Higher: ");
                    String gdzie = sc.nextLine();
                    switch (gdzie) {
                        case "1":
                            s.config.lower.add(conf);
                            break;
                        case "2":
                            s.config.side.add(conf);
                            break;
                        case "3":
                            s.config.higher.add(conf);
                            break;
                        default:
                            try {
                                throw new IllegalStateException("Wrong choice: " + option + "\nConfig not added!");
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                                continue;
                            }
                    }
                    log("Added.");
                    break;

                case 2: // podłącz
                    s.connectToAll();
                    break;

                case 3: // dodaj hasz do zapisania
                    s.addHash();
                    break;

                case 4: // wyślij posiadane drzewo w górę
                    s.sendRootUpwards();
                    break;
                default:
                    try {
                        throw new IllegalStateException("Wrong choice: " + option);
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}
