package pl.kits;

import pl.kits.classes.*;

import static pl.kits.Tests.*;
import static pl.kits.classes.Log.*;

import pl.kits.communication.LowNode;
import pl.kits.communication.NodeConfig;
import pl.kits.communication.Server;
import pl.kits.database.BranchDTO;
import pl.kits.database.Connection;

public class Main {

    public static void main(String[] args) throws Exception {

        BranchDTO.clear(new Connection());

        UI ui = new UI();
        ui.InteractiveNode();

        partialRootDb();
        NodeConfig config = new NodeConfig("node1");
        NodeConfig config1 = new NodeConfig("node2");

        config.IP = "localhost";
        config1.IP = "localhost";

        config.port = 9001;
        config1.port = 9002;

        config.side.add(config1);
        config1.side.add(config);

        Branch a = deterministic();
        Branch b = deterministic();

        Server n1 = new LowNode(config, a);

        Server n2 = new LowNode(config1);
        n1.start();
        n2.start();
        n2.connect(config);
        Thread.sleep(100);

        n2.root = n2.clients.get(0).getRoot();
        log("ROOT VERIFY: " + n2.root.verify());

        n2.clients.get(0).sendHash(Sha256.hash("new leaf"));
        log("N1 new leaf ver: " + n1.root.verify());
        n2.clients.get(0).sendBranch(b);

        Thread.sleep(5000);
        n2.close();
        n1.close();
    }


}