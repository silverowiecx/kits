package pl.kits.classes;

public class Log {

    /**
     * Print message
     *
     * @param message String to be printed
     */
    public static synchronized void log(String message) {
        System.out.println(message);
    }

    /**
     * Print message
     * @param message Integer to be printed
     */
    public static synchronized void log(int message) {
        System.out.println(message);
    }

    /**
     * Print message
     * @param message Boolean to be printed
     */
    public static synchronized void log(boolean message) {
        System.out.println(message);
    }

    /**
     * Print message
     * @param message Long to be printed
     */
    public static synchronized void log(long message) {
        System.out.println(message);
    }


    /**
     * Print red message
     * @param message String to be printed
     */
    public static synchronized void logError(String message) {
        System.err.println(message);
    }

    /**
     * Print red message
     * @param message Integer to be printed
     */
    public static synchronized void logError(int message) {
        System.err.println(message);
    }

    /**
     * Print red message
     * @param message Long to be printed
     */
    public static synchronized void logError(long message) {
        System.err.println(message);
    }

    /**
     * Print red message
     * @param message Boolean to be printed
     */
    public static synchronized void logError(boolean message) {
        System.err.println(message);
    }


}
