package pl.kits.classes;


import java.nio.ByteBuffer;
import java.util.Arrays;


public class ByteUtils {
    /**
     * Helper array for hexadecimal encoding
     */
    private final static char[] hexArray = "0123456789abcdef".toCharArray();

    /**
     * Converts unsigned integer to 4-byte array
     *
     * @param value "unsigned integer" of type long
     * @return 4-byte array
     */
    public static byte[] unsignedToBytes(long value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putLong(value);

        return Arrays.copyOfRange(bytes, 4, 8);
    }

    /**
     * Converts 4-byte array to "unsigned integer" of type long
     * @param bytes 4-byte array
     * @return "unsigned integer" of type long
     */
    public static long bytesToUnsigned(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8).put(new byte[]{0, 0, 0, 0}).put(bytes);
        buffer.position(0);

        return buffer.getLong();
    }

    /**
     * Converts bytes to hexadecimal string
     * @param bytes to be converted
     * @return string
     */
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Converts hexadecimal string to byte array
     *
     * @param str hex string
     * @return byte array
     */
    public static byte[] hexToBytes(String str) {
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer
                    .parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }


    /**
     * Changes endianness of an array
     *
     * @param array to be reversed
     * @return reversed array
     */
    public static byte[] switchEndianness(byte[] array) {
        if (array == null) {
            return null;
        }
        byte[] temp = new byte[array.length];
        System.arraycopy(array, 0, temp, 0, array.length);
        int i = 0;
        int j = temp.length - 1;
        byte tmp;
        while (j > i) {
            tmp = temp[j];
            temp[j] = temp[i];
            temp[i] = tmp;
            j--;
            i++;
        }
        return temp;
    }


}

