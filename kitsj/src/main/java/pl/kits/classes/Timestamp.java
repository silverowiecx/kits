package pl.kits.classes;


/**
 * Container for Timestamp methods
 */
public class Timestamp {
    /**
     * Gets current Timestamp.
     * In production version to be replaced by timestamping system.
     *
     * @return 4 byte array timestamp
     */
    public static byte[] get() {
        return ByteUtils.unsignedToBytes(System.currentTimeMillis() / 1000);
    }
}