package pl.kits.classes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static pl.kits.classes.ByteUtils.switchEndianness;

/**
 * Container for hashing methods
 */
public class Sha256 {

    /**
     * @param rootHash  current roothash
     * @param hash      hash to be added
     * @param timestamp timestamp to consider
     * @return SHA-256 hashed result of parameters
     */
    public static byte[] hash(byte[] rootHash, byte[] hash, byte[] timestamp) {
        byte[] data = new byte[68];
        System.arraycopy(rootHash, 0, data, 0, 32);
        System.arraycopy(hash, 0, data, 32, 32);
        System.arraycopy(timestamp, 0, data, 64, 4);
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            return messageDigest.digest(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Encrypt any string
     *
     * @param toHash String to be hashed
     * @return 32 byte array of hash
     */
    public static byte[] hash(String toHash) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] dig = messageDigest.digest(toHash.getBytes());
            //switchEndianness(dig);
            return dig;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}