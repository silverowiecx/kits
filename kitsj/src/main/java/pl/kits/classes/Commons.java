package pl.kits.classes;

/**
 * Constants container
 */
public class Commons {
    public static final byte[] Leaf = {76};     // L
    public static final byte[] Branch = {66};   // B
    public static final byte[] Root = {82};     // R
    public static final byte[] Cnt = {67};      // C
}