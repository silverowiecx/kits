package pl.kits.classes;


import pl.kits.interfaces.IBranch;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Branch contains list of Leaves and list of Branches that are under it.
 * Each Leaf or Branch joined changes the root hash of this Branch.
 * Joining a Branch results in adding a Leaf with type Branch.
 * Regular hashes are added as Leaf
 */
public class Branch implements IBranch {
    public static LinkedList<String> path = new LinkedList<>();
    private final Object locker = new Object();
    public List<Leaf> leaves;
    public List<Branch> branches;

    /**
     * New, empty Branch
     */
    public Branch() {
        leaves = new LinkedList<>();
        branches = new LinkedList<>();
    }

    /**
     * Creates new Branch from data stream
     *
     * @param stream stream with Branch data
     */
    public Branch(InputStream stream) {
        leaves = new LinkedList<>();
        branches = new LinkedList<>();
        synchronized (locker) {
            try {
                byte[] start = new byte[1];
                stream.read(start);
                if (!Arrays.equals(start, Commons.Root))
                    throw new Exception("Root expected in data stream!");
                byte[] blen = new byte[4];
                stream.read(blen);
                long len = ByteUtils.bytesToUnsigned(ByteUtils.switchEndianness(blen));
                for (int i = 0; i < len; i++) {
                    byte[] bleaf = new byte[69];
                    stream.read(bleaf);
                    Leaf l = new Leaf(bleaf);
                    leaves.add(l);
                }

                for (Leaf l : leaves) {
                    if (Arrays.equals(l.type, Commons.Branch)) {
                        Branch level = new Branch(stream);
                        branches.add(level);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public int totalLeafCount() {
        int c = leaves.size();
        for (Branch branch : branches) {
            c += branch.totalLeafCount();
        }
        return c;
    }


    public int serializedSize() {
        int c = leaves.size() * 69;  // każdy liść waży 69 bajtów
        for (Branch branch : branches) {
            c += branch.serializedSize();
        }
        return c + 5;               // każdy branch zaczyna się od bajtu "R" i 4 bajtów ilości liści
    }

    public int serializedRootSize() {
        return leaves.size() * 69 + 5;
    }


    public byte[] addLeaf(byte[] hash, byte[] timestamp) throws Exception {
        synchronized (locker) {
            if (hash.length != 32 || timestamp.length != 4)
                throw new Exception("Bad data size for adding Leaf!");
            Leaf l = new Leaf(hash, timestamp);
            l.rootHash = Sha256.hash(rootHash(), l.hash, l.timestamp);
            leaves.add(l);
            return rootHash();
        }
    }


    public byte[] addBranch(Branch branch, byte[] timestamp) throws Exception {
        synchronized (locker) {
            if (branch == null || timestamp.length != 4)
                throw new Exception("Null branch or wrong timestamp length for adding Branch!");
            if (!verifyBranch(branch)) throw new Exception("Given Branch is faulty!");
            Leaf l = new Leaf(branch, timestamp);
            l.rootHash = Sha256.hash(rootHash(), l.hash, l.timestamp);
            this.leaves.add(l);
            branches.add(branch);
            return rootHash();
        }
    }


    public byte[] addBranch(InputStream stream) throws Exception {
        synchronized (locker) {
            Branch b = new Branch(stream);
            if (verifyBranch(b))
                return addBranch(b, Timestamp.get());
            else throw new Exception("Can't add broken branch!");
        }
    }

    /**
     * Creates new root Branch from input stream
     *
     * @param stream input stream with root Branch
     * @return created Branch
     */
    public static Branch onlyRoot(InputStream stream) {
        try {
            Branch b = new Branch();
            byte[] id = new byte[1];
            stream.read(id);

            if (Arrays.equals(id, Commons.Root)) {
                byte[] bile = new byte[4];
                stream.read(bile);
                long ile = ByteUtils.bytesToUnsigned(ByteUtils.switchEndianness(bile));
                for (long i = 0; i < ile; i++) {
                    byte[] bleaf = new byte[69];
                    stream.read(bleaf);
                    b.leaves.add(new Leaf(bleaf));
                }
            }
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public byte[] find(byte[] hash) {
        synchronized (locker) {
            return find(hash, this);
        }
    }

    /**
     * Find a hash in given Branch and return the
     * Branch that is a root of minimum tree containing that hash
     *
     * @param hash   hash to be found
     * @param branch Branch to be searched in
     * @return Branch - root of minimum tree containing hash
     */
    private Branch proofFind(byte[] hash, Branch branch) throws Exception {
        path.add(ByteUtils.bytesToHex(branch.rootHash()));
        if (branch.branches.size() > 0) {
            for (Branch b : branch.branches) {
                Branch last = b.proofFind(hash);
                if (!Arrays.equals(last.rootHash(), new byte[32])) {
                    Branch upper = new Branch();
                    for (Leaf l : branch.leaves) {
                        Leaf temp = new Leaf(l.hash, l.timestamp);
                        temp.rootHash = l.rootHash;
                        if (Arrays.equals(temp.hash, last.rootHash()))
                            temp.type = Commons.Branch;
                        upper.leaves.add(temp);
                    }
                    upper.branches.add(last);
                    return upper;
                }
            }
        } else {
            Leaf toFind = branch.leaves.stream().filter(l -> Arrays.equals(l.hash, hash))
                    .findFirst().orElse(null);
            if (toFind != null) {
                return branch;
            }
        }
        Branch.path.pollLast();
        return new Branch();
    }


    public Branch proofFind(byte[] hash) throws Exception {
        synchronized (locker) {
            return proofFind(hash, this);
        }
    }

    //region Serializacja

    /**
     * Finds hash in given Branch.
     *
     * @param hash   Hash to be found
     * @param branch Branch to be search in
     * @return 4 byte array timestamp, or byte[0] when not found
     */
    private byte[] find(byte[] hash, Branch branch) {
        if (branch.branches.size() > 0) {
            for (Branch b : branch.branches) {
                byte[] ts = find(hash, b);
                if (ts.length > 0)
                    return ts;
            }
        } else {
            Leaf toFind = branch.leaves.stream().filter(l -> Arrays.equals(l.hash, hash))
                    .findFirst().orElse(null);
            if (toFind != null)
                return toFind.timestamp;
        }

        return new byte[0];
    }


    public void serialize(OutputStream stream) throws Exception {
        synchronized (locker) {
            serialize(rootHash(), this, stream);
        }
    }


    public void serialize(byte[] hash, OutputStream stream) throws Exception {
        synchronized (locker) {
            if (hash.length != 32) throw new Exception("Hash needs to be 32 bytes (256bits) long!");
            serialize(hash, this, stream);
        }
    }


    public void serialize(byte[] hash, Branch branch, OutputStream stream) throws Exception {
        if (Arrays.equals(hash, branch.rootHash())) {
            branch.serializeRoot(stream);
            for (Branch b : branch.branches)
                b.serialize(stream);
        } else {
            for (Branch b : branch.branches)
                b.serialize(hash, stream);
        }
    }


    public void serializeRoot(OutputStream stream) throws Exception {
        synchronized (locker) {
            serializeRoot(this, stream);
        }
    }

    /**
     * Serializes root of concrete Branch object to stream
     *
     * @param branch Branch to be processed
     * @param stream stream for output
     * @throws Exception
     */
    private void serializeRoot(Branch branch, OutputStream stream) throws Exception {
        stream.write(Commons.Root);
        byte[] numLeaves = ByteUtils.unsignedToBytes(branch.leaves.size());
        stream.write(ByteUtils.switchEndianness(numLeaves));
        for (Leaf l : branch.leaves) {
            byte[] leaf = l.serialize();
            try {
                stream.write(leaf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void serializeRootFrom(byte[] hash, OutputStream stream) {
        synchronized (locker) {
            serializeRootFrom(hash, this, stream);
        }
    }

    /**
     * Sends root of given hash to the end
     *
     * @param hash   beginning hash
     * @param branch Branch to be searched on
     * @param stream stream for output
     */
    private void serializeRootFrom(byte[] hash, Branch branch, OutputStream stream) {
        Leaf toFind = branch.leaves.stream().filter(l -> l.rootHash == hash)
                .findFirst().orElse(null);
        int idx = (branch.leaves.indexOf(toFind));
        try {
            if (toFind != null) {
                stream.write(Commons.Root);
                byte[] idb = ByteUtils.unsignedToBytes(branch.leaves.size() - (idx + 1)); // size - idx oznacza o jeden lisc za duzo, stad +1
                stream.write(ByteUtils.switchEndianness(idb));
                for (int i = idx + 1; i < branch.leaves.size(); i++) {
                    byte[] leaf = branch.leaves.get(i).serialize();
                    stream.write(leaf);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void serializeRoot(byte[] hash, OutputStream stream) throws Exception {
        synchronized (locker) {
            Leaf toFind = leaves.stream().filter(l -> l.rootHash == hash)
                    .findFirst().orElse(null);
            int idx = (leaves.indexOf(toFind));
            if (idx >= 0) {
                stream.write(Commons.Root);

                int ile = leaves.size() - idx;
                byte[] bile = ByteUtils.unsignedToBytes(ile);
                stream.write(ByteUtils.switchEndianness(bile));

                for (int i = 0; i < ile; i++) {
                    byte[] leaf = leaves.get(idx + i).serialize();
                    stream.write(leaf);
                }
            }
        }
    }


    public void serializeBranch(byte[] hash, OutputStream stream) {
        synchronized (locker) {
            serializeBranch(hash, this, stream);
        }
    }

    public void serializeBranch(byte[] hash, Branch branch, OutputStream stream) {
        Branch toFind = branch.branches.stream().filter(b -> b.rootHash() == hash)
                .findFirst().orElse(null);
        int idx = (branch.branches.indexOf(toFind));
        if (idx >= 0) {
            try {
                branch.branches.get(idx).serialize(stream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            for (Branch b : branch.branches)
                serializeBranch(hash, b, stream);
        }
    }

    //endregion

    //region Deserializacja

    public Branch findBranch(byte[] hash) {
        if (Arrays.equals(hash, rootHash())) {
            return this;
        } else {
            for (int bc = 0; bc < branches.size(); bc++) {
                Branch b = branches.get(bc).findBranch(hash);
                if (b.leaves.size() > 0) return b;
            }
        }
        return new Branch();
    }

    public byte[] joinRoot(InputStream stream) {
            try {
                Branch toAdd = new Branch();
                byte[] rh = rootHash();
                byte[] id = new byte[1];
                stream.read(id);
                if (Arrays.equals(id, Commons.Root)) {
                    byte[] bnum = new byte[4];
                    stream.read(bnum);
                    long num = ByteUtils.bytesToUnsigned(ByteUtils.switchEndianness(bnum));
                    for (int i = 0; i < num; i++) {
                        byte[] bleaf = new byte[69];
                        stream.read(bleaf);
                        Leaf l = new Leaf(bleaf);
                        byte[] hash = Sha256.hash(rh, l.hash, l.timestamp);
                        if (Arrays.equals(hash, l.rootHash)) {
                            toAdd.leaves.add(l);
                            rh = toAdd.rootHash();
                        } else return new byte[0];
                    }
                    synchronized (this) {
                        leaves.addAll(toAdd.leaves);
                    }
                    return rootHash();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        return new byte[0];
    }
    //endregion

    //region Weryfikacja

    /**
     * Verifies hashes in root of given Branch
     *
     * @param branch Branch to be verified
     * @return true/false depending on verify success/fail
     */
    private boolean verifyRoot(Branch branch) {
        if (branch.leaves.size() > 0) {
            byte[] myRoot = new byte[32];
            byte[] hash;
            for (Leaf l : branch.leaves) {
                hash = Sha256.hash(myRoot, l.hash, l.timestamp);
                if (Arrays.equals(hash, l.rootHash))
                    myRoot = hash;
                else return false;
            }
        }
        return true;
    }


    public boolean verifyBranch(Branch branch) {
        if (verifyRoot(branch)) {
            if (branch.branches.size() > 0) {
                for (Branch b : branch.branches)
                    if (!verifyBranch(b)) return false;
            }
            return true;
        } else return false;
    }

    public boolean verify() {
        synchronized (locker) {
            return verifyBranch(this);
        }
    }

    public boolean verifyRoot() {
        synchronized (locker) {
            return verifyRoot(this);
        }
    }

    public boolean verifyRootPart() {
        if (leaves.size() > 0) {
            byte[] myRoot = leaves.get(0).rootHash;
            for (int lc = 1; lc < leaves.size(); lc++) {
                Leaf l = leaves.get(lc);
                byte[] hash = Sha256.hash(myRoot, l.hash, l.timestamp);
                if (Arrays.equals(hash, l.rootHash)) {
                    myRoot = hash;
                } else return false;
            }
        } else return false;

        return true;
    }
    //endregion


    public byte[] rootHash() {
        return leaves.size() < 1 ? new byte[32] : leaves.get(leaves.size() - 1).rootHash;
    }

    public int numberOfRootLeaves() {
        return leaves.size();
    }


}