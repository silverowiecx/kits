package pl.kits.classes;


import java.util.Arrays;

/**
 * Main unit holding a hash and a timestamp.
 * Can be a data leaf with a hash or a Branch representative
 */
public class Leaf {
    public byte[] rootHash;
    public byte[] hash;
    public byte[] timestamp;
    public byte[] type;

    /**
     * Constructor - Leaf from some data
     *
     * @param hash      Hash to be saved
     * @param timestamp Timestamp to be saved
     * @throws Exception if hash (32B) or timestamp(4B) array length inappropiate
     */
    public Leaf(byte[] hash, byte[] timestamp) throws Exception {
        if (hash.length != 32 || timestamp.length != 4) throw new Exception("Bad data size for Leaf creation!");
        this.hash = hash;
        this.timestamp = timestamp;
        this.rootHash = new byte[32];
        this.type = Commons.Leaf;
    }

    /**
     * Constructor - Leaf from Branch
     *
     * @param branch    Branch to be joined
     * @param timestamp Timestamp to be added
     * @throws Exception when Branch null or timestamp length inappropriate
     */
    public Leaf(Branch branch, byte[] timestamp) throws Exception {
        if (branch == null || timestamp.length != 4) throw new Exception("Null branch or wrong timestamp length!");
        this.hash = branch.rootHash();
        this.timestamp = timestamp;
        this.rootHash = new byte[32];
        this.type = Commons.Branch;

    }

    /**
     * Constructor - Leaf from byte array
     *
     * @param data byte array of length 69B (type + hash + timestamp + roothash)
     * @throws Exception when failed
     */
    public Leaf(byte[] data) throws Exception {
        if (data.length != 69)
            throw new Exception("Bad data size fo Leaf creation!");
        this.type = new byte[1];
        System.arraycopy(data, 0, type, 0, 1);
        if (!(Arrays.equals(type, Commons.Branch) || Arrays.equals(type, Commons.Leaf)))
            throw new Exception("Wrong Leaf type!");
        hash = new byte[32];
        System.arraycopy(data, 1, hash, 0, 32);
        timestamp = new byte[4];
        System.arraycopy(data, 33, timestamp, 0, 4);
        rootHash = new byte[32];
        System.arraycopy(data, 37, rootHash, 0, 32);
    }


    /**
     * Constructor - Leaf from Branch abstraction - calculates rootHash
     *
     * @param branch         Branch to be used for Leaf creation
     * @param timestamp      Timestamp to be used for Leaf creation
     * @param curentRootHash current rootHash to be used for hashing rootHash of this Leaf
     */
    public Leaf(Branch branch, byte[] timestamp, byte[] curentRootHash) {
        hash = branch.rootHash();
        this.timestamp = timestamp;
        this.rootHash = Sha256.hash(curentRootHash, hash, timestamp);
        type = Commons.Branch;
    }


    /**
     * Leaf serialization
     *
     * @return Leaf as a byte array (69B - type + hash + timestamp + root hash
     */
    public byte[] serialize() {
        byte[] data = new byte[69]; // 1+32+4+32
        System.arraycopy(type, 0, data, 0, 1);
        System.arraycopy(hash, 0, data, 1, 32);
        System.arraycopy(timestamp, 0, data, 33, 4);
        System.arraycopy(rootHash, 0, data, 37, 32);
        return data;
    }
}