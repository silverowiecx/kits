from typing import overload

import MySQLdb
from MySQLdb import cursors

from kits.classes import Leaf, Commons, Branch, Timestamp, Sha256, log

MYSQL_HOST = "localhost"
MYSQL_USER = "root"
with open('p', 'r') as f:
    MYSQL_PASSWORD = str(f.read())
DB_NAME = "KITS"


def connect():
    """Gets connection object"""
    try:
        con = MySQLdb.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD,
                              db=DB_NAME, cursorclass=cursors.DictCursor)
        con.autocommit(False)
        return con
    except Exception as e:
        log(e, "red")


def get_root_hash(c):
    """
    Gets rootHash of main Branch
    :param c: connection to be used
    :return: roothash of root
    """
    command = "CALL GET_ROOTHASH();"
    cursor = c.cursor()
    cursor.execute(command)
    result = cursor.fetchone()
    return bytearray(result['roothash']) if result is not None else bytearray(32)


def save_leaf(c, l, parent_hash):
    """
    Inserts Leaf in database
    :param c: opened connection
    :param l: leaf to be added
    :param parent_hash: hash of Branch where Leaf should be placed
    :return: index where leaf was put or 0 when failed
    """
    if parent_hash is None:
        sql_add_leaf_command = "CALL INSERT_LEAF(%b, %b, %b, %b, NULL);"
        leaf_tuple = (bytes(l.type), bytes(l.root_hash), bytes(l.hash), bytes(l.timestamp))
    else:
        sql_add_leaf_command = "CALL INSERT_LEAF(%b, %b, %b, %b, %b);"
        leaf_tuple = (bytes(l.type), bytes(l.root_hash), bytes(l.hash), bytes(l.timestamp), bytes(parent_hash))
    try:
        cursor = c.cursor()
        result = cursor.execute(sql_add_leaf_command, leaf_tuple)
        if result < 1:
            raise Exception("Error while adding leaf!")
        return result
    except Exception as e:
        c.rollback()
        log(e, "red")
        return 0


def serialize_tree_object(root):
    """
    Saves the whole tree in database
    :param root: root of the tree to be saved
    """
    c = connect()
    for l in root.leaves:
        save_leaf(c, l, None)
    for b in root.branches:
        serialize_branch_object(c, b)
    c.commit()


def serialize_branch_object(c, branch):
    """
    Saves the tree under given branch in database
    :param c: opened connection
    :param branch: Branch where the given tree starts
    """
    if c is None:
        c = connect()
    for l in branch.leaves:
        save_leaf(c, l, branch.root_hash)
    for b in branch.branches:
        serialize_branch_object(c, b)


def get_leaf(c, hash):
    """
    Retrieves Leaf by hash
    :param c: connection to be used
    :param hash: hash of the Leaf to be found
    :return: Retrieved Leaf
    """
    try:
        sql_get_root_leaves = "CALL GET_LEAF(%b);"
        c = c.connect()
        cursor = c.cursor()
        cursor.execute(sql_get_root_leaves, (hash,))
        sqleaf = cursor.fetchone()
        ts = bytearray(sqleaf["timestamp"])
        hash = bytearray(sqleaf["hash"])
        root_hash = bytearray(sqleaf["roothash"])
        type = bytearray(sqleaf["type"])
        l = Leaf(hash=hash, timestamp=ts)
        l.type = type
        l.root_hash = root_hash
        return l
    except Exception as e:
        log(e, "red")
        return None


def only_root(c, b, parent):
    """
    Adds only root Leaves without recursion
    :param c: connection to be used
    :param b: branch to be added
    :param parent: hash of paren to which branch belongs
    :return: void
    """
    for l in b.leaves:
        save_leaf(c, l, parent)


def add_branch(c, b, parent):
    """
    Adds Branch under desired parent
    :param c: connecion to be used
    :param b: branch to be added
    :param parent: parent under which Branch should be added
    :return: roothash
    """
    current_rh = get_root_hash(c)
    leaf = Leaf.leaf_from_branch(b, Timestamp.get(), current_rh)
    rh = add_leaf(c, leaf, None)
    test_leaf = get_leaf(c, b.root_hash)
    if test_leaf.hash == parent:
        c.begin()
        only_root(c, b, parent)
        c.commit()
        for bc in b.branches:
            add_branch(c, bc, bc.root_hash)
        return rh
    else:
        log("AB: parent not found!", "red")
        return None


def serialize_root_from(c, s, from_hash):
    """
    Sends root of given hash to the end
    :param c: connection to be used
    :param s: stream for output
    :param from_hash: beginning hash
    :return:
    """
    statement = "CALL GET_ROOT_LEAVES_FROM(%b);"
    cursor = c.cursor()
    cursor.execute(statement, (bytes(from_hash),))
    result = cursor.fetchall()
    b = Branch()
    for sqleaf in result:
        ts = bytearray(sqleaf["timestamp"])
        hash = bytearray(sqleaf["hash"])
        root_hash = bytearray(sqleaf["roothash"])
        type = bytearray(sqleaf["type"])
        l = Leaf(hash=hash, timestamp=ts)
        l.type = type
        l.root_hash = root_hash
        b.leaves.append(l)
    b.serialize_root(stream=s)


def join_root(c, stream):
    """
     Joins root into Branch from input stream. Each new Leaf
     is verified by hash and if it fails, it stops and returns current
     roothash.
    :param c: connection to be used
    :param stream: stream to be used for deserialization
    :return: extended Branch roothash or byte[0] when failed
    """
    rh = get_root_hash(c)
    id = bytearray(stream.read(1))
    if id == Commons.root:
        bnum = int.from_bytes(bytearray(stream.read(4)), 'little')
        for i in range(bnum):
            bleaf = bytearray(stream.read(69))
            l = Leaf(data=bleaf)
            hash = Sha256.hash(rh, l.hash, l.timestamp)
            if hash == l.root_hash:
                save_leaf(c, l, None)
                rh = get_root_hash(c)
            else:
                c.rollback()
                return bytearray(0)
        return get_root_hash(c)
    return bytearray(0)


def save_from_file(c, s, parent=None):
    """
        Reads file from stream and inserts tree into db
        :param c: opened connection
        :param s: stream with tree to be inserted
        :param parent: None
    """
    temp = Branch()
    start = s.read(1)
    if start != Commons.root:
        raise Exception("Root expected in data stream!")
    blen = int.from_bytes(s.read(4), 'little')
    branch_hashes = list()
    for i in range(blen):
        l = Leaf(data=s.read(69))
        if l.type == Commons.branch:
            branch_hashes.append(l.hash)
        temp.leaves.append(l)
    if not temp.verify_root():
        raise Exception("Particular Branch verify failed!")
    else:
        for l in temp.leaves:
            save_leaf(c, l, parent)
        c.commit()
        for b in branch_hashes:
            save_from_file(c, s, b)


def serialize_tree(stream):
    """
    Loads the whole tree from database and writes it into output stream
    :param stream: opened output stream
    """
    root = Branch()
    sql_get_root_leaves = "CALL GET_ROOT_LEAVES();"
    try:
        c = connect()
        cursor = c.cursor()
        cursor.execute(sql_get_root_leaves)
        result = cursor.fetchall()
        for sqleaf in result:
            ts = bytearray(sqleaf["timestamp"])
            hash = bytearray(sqleaf["hash"])
            root_hash = bytearray(sqleaf["roothash"])
            type = bytearray(sqleaf["type"])
            l = Leaf(hash=hash, timestamp=ts)
            l.type = type
            l.root_hash = root_hash
            root.leaves.append(l)
        if not root.verify_root():
            raise Exception("Particular Branch verify failed!")
        else:
            root.serialize_root(stream=stream)
            for l in root.leaves:
                if l.type == Commons.branch:
                    serialize_branch(c, stream, l.hash)
    except Exception as e:
        log(e, "red")


def serialize_branch(c, stream, parent_hash):
    """
    Loads Branch of given id from database and writes it into output stream
    :param c: opened database connection
    :param stream: opened output stream
    :param parent_hash: id of Branch to be loaded
    """
    branch = Branch()
    sql_get_leaves = "CALL GET_LEAVES(%b);"
    try:
        cursor = c.cursor()
        cursor.execute(sql_get_leaves, (bytes(parent_hash),))
        result = cursor.fetchall()
        for sqleaf in result:
            ts = bytearray(sqleaf["timestamp"])
            hash = bytearray(sqleaf["hash"])
            root_hash = bytearray(sqleaf["roothash"])
            type = bytearray(sqleaf["type"])
            l = Leaf(hash=hash, timestamp=ts)
            l.type = type
            l.root_hash = root_hash
            branch.leaves.append(l)
        if not branch.verify_root():
            raise Exception("Particular Branch verify failed!")
        else:
            branch.serialize_root(stream=stream)
            for l in branch.leaves:
                if l.type == Commons.branch:
                    serialize_branch(c, stream, l.hash)
    except Exception as e:
        log(e, "red")


def total_leaf_count(c):
    """
    Simply counts number of leaves in database
    :param c: opened connection
    :return: leaf count
    """
    try:
        cursor = c.cursor()
        cursor.execute("CALL TOTAL_LEAF_COUNT();")
        result = cursor.fetchone()
        return result["COUNT(*)"]
    except Exception as e:
        log(e, "red")
        return 0


def add_leaf(c, l=None, parent_hash=None, hash=None, timestamp=None):
    """
    Adds Leaf or Branch under the given parent
    :param c: opened connection
    :param l: Leaf of any type to be added
    :param parent_hash: Hash of Branch where the given Leaf should be added
    :return: new root hash of branch where leaf was added
    """
    if l is not None:
        try:
            save_leaf(c, l, parent_hash)
            c.commit()
            cursor = c.cursor()
            cursor.execute("CALL ROOT_HASH(%b);", (bytes(parent_hash),))
            result = cursor.fetchall()
            return bytearray(result["roothash"])
        except Exception as e:
            log(e, "red")
            return bytearray(32)
    else:
        try:
            l = Leaf(hash=hash, timestamp=timestamp)
            l.root_hash = Sha256.hash(root_hash=get_root_hash(c), hash=hash, timestamp=timestamp)
            save_leaf(c, l, None)
            return get_root_hash(c)
        except Exception as e:
            log(e, "red")
            return bytearray(0)


def find(c, hash):
    """
    Finds a timestamp of given hash
    :param c: opened connection
    :param hash: hash which timestamp should be found
    :return: found timestamp or bytearray(4)
    """
    try:
        cursor = c.cursor()
        cursor.execute("CALL FIND(%b);", (bytes(hash),))
        result = cursor.fetchone()
        return result["timestamp"]
    except Exception as e:
        log(e, "red")
    return bytearray(4)


def proof_find(c, hash, child=None, parent=None):
    """
    Recursive down-up rebuild of path to desired hash. Branch should be verified afterwards.
    :param c: opened connection
    :param hash: hash to be found in database
    :param child: child branch to be added, should be None during invocation
    :param parent:  id of database parent, should be None during invocation
    :return:
    """
    if child is None:
        sql = "CALL PROOF_DEEP_BRANCH(%b);"
        cursor = c.cursor()
        cursor.execute(sql, (bytes(hash),))
        result = cursor.fetchall()
        temp = Branch()
        for r in result:
            l = Leaf(bytearray(r['hash']), bytearray(r['timestamp']))
            l.root_hash = bytearray(r['roothash'])
            temp.leaves.append(l)
        return proof_find(c, hash, temp, int(result[0]['parent']))
    else:
        if parent is None:
            return child
        temp = Branch()
        sql = "CALL PROOF_UPPER_PARENT(%s);"
        cursor = c.cursor()
        cursor.execute(sql, (int(parent),))
        result = cursor.fetchone()
        upper_parent = result['parent']
        if upper_parent is None or upper_parent < 1:
            sql = "CALL GET_ROOT_LEAVES();"
        else:
            sql = "CALL GET_LEAVES_BY_ID({});".format(upper_parent)
        cursor = c.cursor()
        cursor.execute(sql)
        result = cursor.fetchall()
        for r in result:
            l = Leaf(bytearray(r['hash']), bytearray(r['timestamp']))
            l.root_hash = bytearray(r['roothash'])
            if r['id_leaf'] == parent:
                l.type = Commons.branch
            temp.leaves.append(l)
        temp.branches.append(child)
        if upper_parent is None or upper_parent < 1:
            return temp
        else:
            return proof_find(c, hash, temp, upper_parent)


def verify():
    """
    Verifies tree in database
    :return: result of verification
    """
    root = Branch()
    sql_get_root_leaves = "CALL GET_ROOT_LEAVES();"
    c = connect()
    cursor = c.cursor()
    cursor.execute(sql_get_root_leaves)
    result = cursor.fetchall()
    for sqleaf in result:
        ts = bytearray(sqleaf["timestamp"])
        hash = bytearray(sqleaf["hash"])
        root_hash = bytearray(sqleaf["roothash"])
        type = bytearray(sqleaf["type"])
        l = Leaf(hash=hash, timestamp=ts)
        l.type = type
        l.root_hash = root_hash
        root.leaves.append(l)
    if not root.verify_root():
        return False
    else:
        for l in root.leaves:
            if not verify_root(c, l.hash):
                return False
        return True


def verify_root(c, parent_hash):
    """
    Verifies particular Branch in database
    :param c: opened database connection
    :param parent_hash: hash of Branch to be verified
    :return: result of verification
    """
    branch = Branch()
    sql_get_root_leaves = "CALL GET_LEAVES(%b);"
    cursor = c.cursor()
    cursor.execute(sql_get_root_leaves, (parent_hash,))
    result = cursor.fetchall()
    for sqleaf in result:
        ts = bytearray(sqleaf["timestamp"])
        hash = bytearray(sqleaf["hash"])
        root_hash = bytearray(sqleaf["roothash"])
        type = bytearray(sqleaf["type"])
        l = Leaf(hash=hash, timestamp=ts)
        l.type = type
        l.root_hash = root_hash
        branch.leaves.append(l)
    if not branch.verify_root():
        return False
    else:
        for l in branch.leaves:
            if not verify_root(c, l.hash):
                return False
        return True
