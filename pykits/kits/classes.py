import hashlib
import io
import threading
import time

from termcolor import colored


def log(msg, color=None):
    if color is None:
        print(msg)
    else:
        print(colored(msg, color))


class Sha256:
    """
    Container for hashing methods
    """

    @staticmethod
    def hash(root_hash, hash, timestamp):
        """
        :param root_hash: current roothash
        :param hash: hash to be added
        :param timestamp: timestamp to consider
        :return: SHA-256 hashed result of parameters
        """
        data = bytearray(68)
        data[0:32] = root_hash[0:32]
        data[32:64] = hash[0:32]
        data[64:68] = timestamp[0:4]
        return bytearray(hashlib.sha256(data).digest())

    @staticmethod
    def to_hash(text):
        """
        hash any text using sha256 algorithm
        :param text: text to be hashed
        :return: 32 byte array hash
        """
        return bytearray(hashlib.sha256(bytearray(text, encoding="UTF-8")).digest())


class Timestamp:
    """
    Container for Timestamp methods
    """
    @staticmethod
    def get():
        """
        Gets current Timestamp.
        In production version to be replaced by timestamping system.
        :return: 4 byte array timestamp
        """
        return bytearray((int(time.time())).to_bytes(byteorder='little', length=4))


class Commons:
    """
    Constants container
    """
    leaf = bytearray(int(76).to_bytes(1, 'little'))  # L
    branch = bytearray(int(66).to_bytes(1, 'little'))  # B
    root = bytearray(int(82).to_bytes(1, 'little'))  # R
    cnt = bytearray(int(67).to_bytes(1, 'little'))  # C


class Leaf:
    """
    Main unit holding a hash and a timestamp.
    Can be a data leaf with a hash or a Branch representative.
    """
    def __init__(self, hash=None, timestamp=bytearray(0), branch=None, data=bytearray(0)):
        """
        Constructor - Leaf from some data
        :param hash: Hash to be saved
        :param timestamp: Timestamp to be saved
        :param branch: Branch to be joined to
        :param data: byte array of length 69 (type + hash + timestamp + roothash)
        """
        if len(data) == 0:
            if hash is not None:
                if len(hash) != 32 or len(timestamp) != 4:
                    raise Exception("Bad data size for Leaf construction!")
                else:
                    self.hash = hash
                    self.timestamp = timestamp
                    self.root_hash = bytearray(32)
                    self.type = Commons.leaf
            else:
                if branch is None or len(timestamp) != 4:
                    raise Exception("Null branch or wrong timestamp length!")
                else:
                    self.hash = branch.root_hash
                    self.timestamp = timestamp
                    self.root_hash = bytearray(32)
                    self.type = Commons.branch
        else:
            if len(data) != 69:
                raise Exception("Bad data size to Leaf construction! {}".format(len(data)))
            else:
                self.type = bytearray(data[0:1])
                if not self.type == Commons.branch and not self.type == Commons.leaf:
                    raise Exception("Wrong Leaf type!")
                else:
                    self.hash = bytearray(data[1:33])
                    self.timestamp = bytearray(data[33:37])
                    self.root_hash = bytearray(data[37:69])

    @staticmethod
    def leaf_from_branch(branch, timestamp, current_root_hash):
        hash = branch.root_hash
        l = Leaf(timestamp=timestamp, hash=hash)
        l.type = Commons.branch
        l.root_hash = Sha256.hash(current_root_hash, hash, timestamp)
        return l

    def serialize(self):
        """
        Leaf serialization
        :return: Leaf as a byte array (69B - type + hash + timestamp + roothash
        """
        data = bytearray(69)
        data[0:1] = self.type[0:1]
        data[1:33] = self.hash[0:32]
        data[33:37] = self.timestamp[0:4]
        data[37:69] = self.root_hash[0:32]
        return data

    def leaf_to_dict(self):
        """UTF representation of Leaf"""
        data = dict()
        data['roothash'] = self.root_hash.hex()
        data['hash'] = self.hash.hex()
        data['timestamp'] = int.from_bytes(self.timestamp, 'little')
        return data

    def __repr__(self):
        return str(self.leaf_to_dict())


class Branch:
    """
    Branch contains list of Leaves and list of Branches that are under it.
    Each Leaf or Branch joined changes the roothash of this Branch.
    Joining a Branch results in adding a Leaf with type Branch.
    Regular hashes are added as Leaf.
    """

    def __init__(self, stream=None):
        """
        New, empty Branch or new Branch from data stream.
        :param stream: stream with Branch data
        """
        self.locker = threading.Lock()
        self.locker.acquire()
        if stream is None:
            self.leaves = list()
            self.branches = list()
        else:
            self.leaves = list()
            self.branches = list()
            start = bytearray(stream.read(1))
            if start != Commons.root:
                raise Exception("Root expected in data stream!")
            else:
                blen = bytearray(stream.read(4))
                ilen = int.from_bytes(blen, 'little')
                for i in range(ilen):
                    bleaf = bytearray(stream.read(69))
                    l = Leaf(data=bleaf)
                    self.leaves.append(l)
                for l in self.leaves:
                    if l.type == Commons.branch:
                        level = Branch(stream=stream)
                        self.branches.append(level)
        self.locker.release()

    @property
    def root_hash(self):
        """getter for root hash"""
        return self.leaves[len(self.leaves) - 1].root_hash if len(self.leaves) > 0 else bytearray(32)

    """Path for hex path print"""
    path = list()

    """Change if you want to get full subtree of this Branch"""
    print_subtree = False

    def branch_to_dict(self, subtree=False):
        """UTF representation of Branch"""
        data = dict()
        data['roothash'] = self.root_hash.hex()
        data['leaves'] = self.leaves
        if Branch.print_subtree:
            data['branches'] = self.branches
        return data

    def __repr__(self):
        return str(self.branch_to_dict(True if Branch.print_subtree else False))

    def add_leaf(self, hash, timestamp):
        """
        Adds Leaf to branch.
        Adds given hash and timestamp to a new Leaf and places it into Branch by
        computing new roothash.
        New roothash is: SHA-256(old roothash + hash + timestamp)
        :param hash: hash to be added
        :param timestamp: timestamp to be considered
        :return: current roothash
        """
        if len(hash) != 32 or len(timestamp) != 4:
            raise Exception("Bad data size for adding Leaf!")
        else:
            self.locker.acquire()
            l = Leaf(hash=hash, timestamp=timestamp)
            l.root_hash = Sha256.hash(root_hash=self.root_hash, hash=hash, timestamp=timestamp)
            self.leaves.append(l)

            self.locker.release()
            return self.root_hash

    def add_branch(self, branch, timestamp, s=None):
        """
        Adds Branch with given timestamp
        or from stream to an existing tree.
        :param branch: Branch to be added
        :param timestamp: Timestamp to be added
        :param s: data input stream
        :return: 32 byte array of new roothash
        """
        if s is None:
            if branch is None or len(timestamp) != 4:
                raise Exception("Null branch or wrong timestamp length for adding Branch!")
            elif not self.verify_branch(branch):
                raise Exception("Given Branch is faulty!")
            else:
                le = Leaf(branch=branch, timestamp=timestamp)
                le.root_hash = Sha256.hash(root_hash=self.root_hash, hash=le.hash, timestamp=le.timestamp)
                self.leaves.append(le)
                self.branches.append(branch)
                return self.root_hash

    def find(self, hash, branch=None):
        """
        Finds hash in given Branch.
        :param hash: Hash to be found
        :param branch: Branch to be searched in
        :return: 4 byte array timestamp or bytearray(0) when not found
        """
        if branch is not None:
            Branch.path.append(branch.root_hash.hex())
            if len(branch.branches) > 0:
                for b in branch.branches:
                    ts = b.find(hash)
                    if ts != bytearray(0):
                        return ts
            else:
                index = next((i for i, item in enumerate(branch.leaves) if item.hash == hash), -1)
                if index >= 0:
                    return branch.leaves[index].timestamp
            Branch.path.pop()
            return bytearray(0)
        else:
            return self.find(hash=hash, branch=self)

    def proof_find(self, hash, branch=None):
        """
        Find a hash in given Branch and return the
        Branch that is a root of minimum tree containing that hash.
        :param hash: hash to be found
        :param branch: Branch to be searched in
        :return: Branch - root of minimum spanning tree containing hash
        """
        if branch is not None:
            Branch.path.append(branch.root_hash.hex())
            if len(branch.branches) > 0:
                for b in branch.branches:
                    last = b.proof_find(hash)
                    if last.root_hash != bytearray(32):
                        upper = Branch()
                        for l in branch.leaves:
                            temp = Leaf(hash=l.hash, timestamp=l.timestamp)
                            temp.root_hash = l.root_hash
                            if temp.hash == last.root_hash:
                                temp.type = Commons.branch
                            upper.leaves.append(l)
                        upper.branches.append(last)
                        return upper
            else:
                index = next((i for i, item in enumerate(branch.leaves) if item.hash == hash), -1)
                if index >= 0:
                    return branch
            Branch.path.pop()
            return Branch()
        else:
            return self.proof_find(hash=hash, branch=self)

    def serialize(self, stream, hash=None, branch=None):
        """
        Serializes Branch to a stream.
        :param stream: stream to be written into
        :param hash: roothash of a Branch
        :param branch: Branch to be searched in
        :return:
        """
        if hash is None and branch is None:
            self.serialize(stream=stream, hash=self.root_hash, branch=self)
        elif isinstance(stream, io.BufferedWriter) and branch is None:
            if len(hash) != 32:
                raise Exception("Hash need to be 32 bytes (256bits) long!")
            elif not stream.writable():
                raise Exception("Cannot write to stream!")
            else:
                self.serialize(stream=stream, hash=self.root_hash, branch=self)
        else:
            if hash == branch.root_hash:
                branch.serialize_root(stream)
                for b in branch.branches:
                    b.serialize(stream=stream)
            else:
                for b in branch.branches:
                    b.serialize(stream=stream, hash=hash)

    def serialize_root(self, stream, branch=None, hash=None):
        """
        Serializes only a root of concrete Branch object of stream
        :param stream: stream for output
        :param branch: Branch to be processed
        :param hash: desired hash
        """
        self.locker.acquire()
        if branch is None and hash is None:
            self.locker.release()
            self.serialize_root(stream, branch=self)
        elif hash is None:
            stream.write(Commons.root)
            num_leaves = bytearray(len(branch.leaves).to_bytes(4, 'little'))
            stream.write(num_leaves)
            for l in branch.leaves:
                leaf = l.serialize()
                stream.write(leaf)
            self.locker.release()
        else:
            index = next((i for i, item in enumerate(branch.leaves) if item.root_hash == hash), -1)
            if index >= 0:
                stream.write(Commons.root)
                leaf_count = bytearray((len(self.leaves) - index).to_bytes(4, 'little'))
                stream.write(leaf_count)
                for i in range(len(self.leaves) - index):
                    stream.write(self.leaves[index + i].serialize())
            self.locker.release()

    def serialize_root_from(self, hash, stream, branch=None):
        """
        Serializes Tree beginning from given hash
        :param hash: beginning hash
        :param stream: stream for output
        :param branch: Branch to be searched in
        """
        self.locker.acquire()
        if branch is None:
            self.locker.release()
            self.serialize_root_from(hash, stream, branch=self)
        else:
            index = next((i for i, item in enumerate(branch.leaves) if item.root_hash == hash), -1)
            if index >= 0:
                stream.write(Commons.root)
                ind = bytearray((len(branch.leaves) - index - 1).to_bytes(4, 'little'))
                stream.write(ind)
                for i in range(index + 1, len(branch.leaves)):
                    stream.write(branch.leaves[i].serialize())
            self.locker.release()

    def serialize_branch(self, hash, stream, branch=None):
        """
        Serializes arbitrary Branch by hash to stream
        :param hash: roothash of desired Branch
        :param stream: stream for output
        :param branch: parent Branch to be searched on
        """
        if branch is None:
            self.serialize_branch(hash, stream, branch=self)
        else:
            self.locker.acquire()
            index = next((i for i, item in enumerate(branch.branches) if item.root_hash == hash), -1)
            if index >= 0:
                branch.branches[index].serialize(stream)
            else:
                for b in branch.branches:
                    self.serialize_branch(hash=hash, branch=b, stream=stream)
            self.locker.release()

    # deserialzacja
    @staticmethod
    def only_root(stream):
        """
        Creates new root Branch from input stream
        :param stream: input stream with root Branch
        :return: created Branch
        """
        b = Branch()
        id = bytearray(stream.read(1))
        if id == Commons.root:
            bile = bytearray(stream.read(4))
            ile = int.from_bytes(bile, 'little')
            for i in range(ile):
                bleaf = bytearray(stream.read(69))
                l = Leaf(data=bleaf)
                b.leaves.append(l)
        return b

    def join_root(self, stream):
        """
        Joins root into Branch from input stream. Each new Leaf
        is verified by hash and if it fails, it stops and returns current
        roothash.
        :param stream: input stream
        :return: new Branch or empty Branch() if wrong data is given
        """
        to_add = Branch()
        rh = self.root_hash
        id = bytearray(stream.read(1))
        if id == Commons.root:
            for i in range(int.from_bytes(stream.read(4), 'little')):
                bleaf = bytearray(stream.read(69))
                l = Leaf(data=bleaf)
                hash = Sha256.hash(root_hash=rh, hash=l.hash, timestamp=l.timestamp)
                if hash == l.root_hash:
                    to_add.leaves.append(l)
                    rh = to_add.root_hash
                else:
                    return bytearray(0)
            self.locker.acquire()
            self.leaves.extend(to_add.leaves)
            self.locker.release()
            return self.root_hash
        return bytearray(0)

    def verify_root(self, branch=None):
        """
        Verifies hashes in root of given Branch
        :param branch: Branch to be verified
        :return: True/False depending on verify success/fail
        """
        if branch is None:
            return self.verify_root(branch=self)
        else:
            if len(branch.leaves) > 0:
                myroot = bytearray(32)
                for l in branch.leaves:
                    hash = Sha256.hash(root_hash=myroot, hash=l.hash, timestamp=l.timestamp)
                    if hash == l.root_hash:
                        myroot = hash
                    else:
                        return False
            return True

    def verify_branch(self, branch):
        """
        Verifies the whole Branch
        :param branch: Branch to be verified
        :return: True/False depending on verify success/fail
        """
        if self.verify_root(branch=branch):
            if len(branch.branches) > 0:
                for b in branch.branches:
                    if not self.verify_branch(b):
                        return False
            return True
        else:
            return False

    def verify(self):
        """
        Verifies itself
        """
        self.locker.acquire()
        b = self.verify_branch(branch=self)
        self.locker.release()
        return b

    def verify_root_part(self):
        """
        Verifies root that does not start with zeros
        :return: result of verification
        """
        if len(self.leaves) > 0:
            my_root = self.leaves[0].root_hash
            for i in range(1, len(self.leaves)):
                l = self.leaves[i]
                h = Sha256.hash(my_root, l.hash, l.timestamp)
                if h == l.root_hash:
                    my_root = h
                else:
                    return False
        else:
            return False
        return True

    @property
    def serialized_size(self):
        c = len(self.leaves) * 69
        for bc in range(len(self.branches)):
            c += self.branches[bc].serialized_size
        return c + 5

    @property
    def serialized_root_size(self):
        return len(self.leaves) * 69 + 5

    @property
    def total_leaf_count(self):
        c = len(self.leaves)
        for b in self.branches:
            c += b.total_leaf_count
        return c
