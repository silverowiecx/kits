import glob
import os
import time
from termcolor import colored
from kits.classes import Sha256, Branch
from kits.communication.server import NodeConfig, Role, Server


def config(to_hash, port, role):
    return NodeConfig(port=port, role=role, to_hash=to_hash)


def get_conf():
    port = int(input("Provide a listening port:"))
    to_hash = input("Provide string to generate Hash ID:")
    role = round(int(input("Provide a Role: 0=Core, 1=Mid, 2=Low:")), 0)
    try:
        return config(to_hash, port, Role(role))
    except Exception as e:
        print(colored("Role chosen incorrectly, Config not saved. Try again:", "red"))
        return get_conf()


def save_unsent_branch(b):
    tmp_dir = "unsentRoot_" + str(int(time.time()))
    with open(tmp_dir, "w+b") as f:
        b.serialize(f)
    print(colored("Unsent root serialized successfully! Filename: {}".format(tmp_dir), "blue"))


def load_unsent_branch_file():
    os.chdir(".")
    unsent_files = list()
    for file in glob.glob("unsentRoot_*"):
        unsent_files.append(file)
    unsent_files.sort()
    if len(unsent_files) < 1:
        return None
    else:
        return unsent_files[0]


def interactive_node():
    print("Starting interactive node...")
    s = Server(node_config=get_conf())
    s.start()
    print("Server is running, what's next?\n")
    koniec = False
    while not koniec:
        option = int(input("Choose command:\n"
                           "0 - Quit\n"
                           "1 - Add Node Configuration to connect to\n"
                           "2 - Connect to all known Nodes\n"
                           "3 - Add hash to be stored\n"
                           "4 - Send Tree upwards\n"))
        if option == 0:  # koniec
            koniec = True
            s.running = False
            print("Closing...")
        elif option == 1:  # dodaj konfig
            conf = get_conf()
            where = int(input("Where to add? 1-Lower, 2-Side, 3-Higher:"))
            if where == 1:
                s.config.lower.append(conf)
            elif where == 2:
                s.config.side.append(conf)
            elif where == 3:
                s.config.higher.add(conf)
            else:
                try:
                    raise Exception("Wrong choice, config not added!")
                except Exception as e:
                    print(e)
                    continue
            print("Added.")
        elif option == 2:  # podłącz
            s.connect_to_all()
        elif option == 3:
            if s.config.role == Role.LOW:
                to_hash = input("Add text to be hashed:")
                h = Sha256.to_hash(to_hash)
                rh = s.add_hash(h)
                print(colored("Current root hash: {}".format(rh.hex()), "green"))
            else:
                print(colored("Only Nodes of type Low can do it.", "red"))
        elif option == 4:  # wyślij posiadane drzewo w górę
            if s.config.role != Role.CORE:
                higher_hashes = list()
                for x in s.config.higher:
                    higher_hashes.append(x.hash)
                to_send = filter(lambda x: x.connected_to in higher_hashes, s.clients)
                if to_send.__sizeof__() < 1:
                    print(colored("There is no Node to send to!", "red"))
                    save_unsent_branch(s.root)
                    s.root = Branch()
                    continue
                awaited_file = load_unsent_branch_file()
                while awaited_file is not None:
                    sent = False
                    with open(awaited_file, 'r+b') as f:
                        awaited = Branch(stream=f)
                    for client in to_send:
                        try:
                            client.send_branch(awaited)
                            sent = True
                            break
                        except Exception as e:
                            print(colored(e, "red"))
                    if not sent:
                        print(colored("Sending awaited root failed! Retrying in a moment...", "red"))
                        time.sleep(1)
                    else:
                        print(colored("Awaited root (ts: {}) sent!".format(awaited_file[11:21]), "green"))
                        os.remove(awaited_file)
                        awaited_file = load_unsent_branch_file()
                sent = False
                for client in to_send:
                    try:
                        client.send_branch(s.root)
                        sent = True
                        break
                    except Exception as e:
                        print(colored(e, "red"))
                if not sent:
                    print(colored("Sending root failed! Storing on disk..."))
                    save_unsent_branch(s.root)
                    s.root = Branch()
                else:
                    print(colored("Root sent!", "green"))
                    # TODO clear root
                    s.root = Branch()
            else:
                print(colored("Core Nodes cannot do that", "red"))
        else:
            print(colored("Wrong choice!", "red"))
