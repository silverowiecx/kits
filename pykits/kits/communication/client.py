import socket
from threading import Thread

from kits.classes import Branch, log

ok = (0).to_bytes(1, 'little')
root_pls = (1).to_bytes(1, 'little')
bye = (2).to_bytes(1, 'little')
root_part = (3).to_bytes(1, 'little')
new_leaf = (4).to_bytes(1, 'little')
new_branch = (5).to_bytes(1, 'little')
branch_pls = (6).to_bytes(1, 'little')


class Client(Thread):
    ports = list(range(9010, 10000))

    def __init__(self, client, server):
        super().__init__()
        self.server = server
        self.client = client
        self.ip = None
        self.connected_to = None

    def process_commands(self):
        while True:
            command = self.client.recv(1)
            port = int.from_bytes(self.client.recv(4), 'little')
            if command == root_pls:
                log("rootPls received", "yellow")
                temp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                temp_client.connect((self.ip, port))
                temp_client.sendall(ok)
                temp_client.sendall(self.server.root.serialized_root_size.to_bytes(4, 'little'))
                self.server.root.serialize_root(temp_client.makefile('wb'))
                if temp_client.recv(1) != bye:
                    raise Exception("No confirmation!")
                temp_client.close()
            elif command == root_part:
                log("rootPart received", "yellow")
                temp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                temp_client.connect((self.ip, port))
                temp_client.sendall(ok)
                hash = bytearray(temp_client.recv(32))
                self.server.send_root(hash, temp_client.makefile('wb'))
                if temp_client.recv(1) != bye:
                    raise Exception("No confirmation!")
                temp_client.close()
            elif command == new_leaf:
                log("newLeaf received", "yellow")
                temp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                temp_client.connect((self.ip, port))
                temp_client.sendall(ok)
                hash = bytearray(temp_client.recv(32))
                buff = bytearray(self.server.add_hash(hash))
                temp_client.sendall(buff)
                if temp_client.recv(1) != bye:
                    raise Exception("No confirmation!")
                temp_client.close()
            elif command == new_branch:
                log("newBranch received", "yellow")
                temp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                temp_client.connect((self.ip, port))
                temp_client.sendall(ok)
                len = int.from_bytes(bytearray(temp_client.recv(4)), 'little')
                b = Branch(stream=temp_client.makefile('rb'))
                hash = bytearray(self.server.add_branch(b))
                temp_client.sendall(bytearray(hash.__sizeof__().to_bytes(4, 'little')))
                temp_client.sendall(hash)
                if temp_client.recv(1) != bye:
                    raise Exception("No confirmation!")
                temp_client.close()
            elif command == branch_pls:
                log("branchPls received", "yellow")
                temp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                temp_client.connect((self.ip, port))
                temp_client.sendall(ok)
                hash = bytearray(temp_client.recv(32))
                buff = bytearray(self.server.send_branch(hash))
                blen = buff.__sizeof__().to_bytes(4, 'little')
                temp_client.sendall(blen)
                temp_client.sendall(buff)
                if temp_client.recv(1) != bye:
                    raise Exception("No confirmation!")
                temp_client.close()
            else:
                print("WTF I JUST READ: ", command.hex())
                self.client.close()

    def close(self):
        self.client.close()

    def connect(self):
        self.client.sendall(bytearray(self.server.config.hash))
        hash = self.client.recv(32)
        ok = False

        for c in self.server.config.higher:
            if hash == c.hash:
                self.ip = c.ip
                ok = True
                break
        for c in self.server.config.lower:
            if hash == c.hash:
                self.ip = c.ip
                ok = True
                break
        for c in self.server.config.side:
            if hash == c.hash:
                self.ip = c.ip
                ok = True
                break
        if ok:
            self.connected_to = hash
            self.process_commands()
        else:
            self.close()

    def get_root(self):
        self.client.sendall(root_pls)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('localhost', Client.ports.pop(0)))
        server_socket.listen(5)
        self.client.sendall(int((server_socket.getsockname()[1])).to_bytes(4, 'little'))
        (client_socket, address) = server_socket.accept()
        Client.ports.append(server_socket.getsockname()[1])
        server_socket.close()
        if client_socket.recv(1) == ok:
            len = int.from_bytes(bytearray(client_socket.recv(4)), 'little')
            b = Branch()
            b = b.only_root(client_socket.makefile("rb"))
            client_socket.sendall(bye)
            return b
        else:
            log("nie dostalem okej", "red")
            return None

    def get_root_part(self, branch, hash):
        self.client.sendall(root_part)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('localhost', Client.ports.pop(0)))
        server_socket.listen(5)
        self.client.sendall(int((server_socket.getsockname()[1])).to_bytes(4, 'little'))
        (client_socket, address) = server_socket.accept()
        Client.ports.append(server_socket.getsockname()[1])
        server_socket.close()
        if client_socket.recv(1) == ok:
            client_socket.sendall(bytearray(hash))
            len = int.from_bytes(bytearray(client_socket.recv(4)), 'little')
            if len > 0:
                branch.join_root(client_socket.makefile('rb'))
                client_socket.sendall(bye)
                return branch
        client_socket.close()
        return None

    def send_branch(self, branch):
        self.client.sendall(new_branch)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('localhost', Client.ports.pop(0)))
        server_socket.listen(5)
        self.client.sendall(int((server_socket.getsockname()[1])).to_bytes(4, 'little'))
        (client_socket, address) = server_socket.accept()
        Client.ports.append(server_socket.getsockname()[1])
        server_socket.close()
        if client_socket.recv(1) == ok:
            client_socket.sendall(branch.serialized_size.to_bytes(4, 'little'))
            branch.serialize(stream=client_socket.makefile('wb'))
            len = int.from_bytes(client_socket.recv(4), 'little')
            if len < 32:
                return bytearray(0)
            hash = bytearray(client_socket.recv(32))
            client_socket.sendall(bye)
            client_socket.close()
            return hash

    def get_branch(self, hash):
        self.client.sendall(branch_pls)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('localhost', Client.ports.pop(0)))
        server_socket.listen(5)
        self.client.sendall(int((server_socket.getsockname()[1])).to_bytes(4, 'little'))
        (client_socket, address) = server_socket.accept()
        Client.ports.append(server_socket.getsockname()[1])
        server_socket.close()
        if client_socket.recv(1) == ok:
            client_socket.sendall(hash)
            len = int.from_bytes(client_socket.recv(4), 'little')
            branch = Branch(client_socket.makefile('rb'))
            client_socket.sendall(bye)
            return branch

    def send_hash(self, hash):
        self.client.sendall(new_leaf)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('localhost', Client.ports.pop(0)))
        server_socket.listen(5)
        self.client.sendall(int((server_socket.getsockname()[1])).to_bytes(4, 'little'))
        (client_socket, address) = server_socket.accept()
        Client.ports.append(server_socket.getsockname()[1])
        server_socket.close()
        if client_socket.recv(1) == ok:
            client_socket.sendall(hash)
            buff = client_socket.recv(32)
            client_socket.sendall(bye)
            return buff

    def run(self):
        self.connect()
