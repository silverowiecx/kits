import io
import socket
from abc import abstractmethod, ABC
from enum import Enum
from threading import Thread
from typing import overload

from kits.classes import Sha256, Branch, Timestamp, Commons
from kits.communication.client import Client


class Role(Enum):
    CORE = 0
    MID = 1
    LOW = 2


class NodeConfig:
    def __init__(self, port, role, hash=None, to_hash=None, ip=None):
        self.higher = list()
        self.lower = list()
        self.side = list()
        self.streams = list()
        self.role = role
        if hash is None and to_hash is None: raise Exception("Hash not provided!")
        if hash is not None:
            self.hash = Sha256.to_hash(hash)
        if to_hash is not None:
            self.hash = Sha256.to_hash(to_hash)
        self.ip = 'localhost' if ip is None else ip
        if port is not None:
            self.port = port


class Server(Thread):
    @abstractmethod
    def __init__(self, node_config=None, branch=None):
        super().__init__()
        self.clients = list()
        self.root = Branch()
        self.running = False
        if node_config is not None:
            self.config = node_config
        if branch is not None:
            self.root = branch
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.config.ip, self.config.port))
        self.server_socket.listen(5)

    @abstractmethod
    def listen(self):
        while self.running:
            (client_socket, address) = self.server_socket.accept()
            client = Client(client_socket, self)
            self.clients.append(client)
            self.clients[len(self.clients) - 1].start()
            print("new incoming connection")
        self.server_socket.close()
        self.running = False

    @abstractmethod
    def connection_exists(self, config):
        for c in self.clients:
            if config.hash == c.connected_to:
                return True
        return False

    @abstractmethod
    def connect(self, config):
        if not self.connection_exists(config):
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.connect((config.ip, config.port))
            client = Client(client_socket, self)
            self.clients.append(client)
            self.clients[len(self.clients) - 1].start()
            print("connected successfully")

    @abstractmethod
    def connect_to_all(self):
        if self.config.role == Role.CORE:
            for c in self.config.side:
                self.connect(c)
        elif self.config.role == Role.LOW:
            for c in self.config.higher:
                self.connect(c)
        else:
            for c in self.config.side:
                self.connect(c)
            for c in self.config.higher:
                self.connect(c)

    def run(self):
        self.running = True
        self.listen()

    @abstractmethod
    def close(self):
        self.running = False
        for c in self.clients:
            c.close()

    @abstractmethod
    def boot(self):
        pass

    @abstractmethod
    def add_branch(self, branch):
        return self.root.add_branch(branch, Timestamp.get())

    @abstractmethod
    def send_current_root_hash(self):
        return self.root.root_hash

    @abstractmethod
    def send_branch(self, hash):
        stream = io.BytesIO()
        self.root.serialize_branch(hash, stream)
        return bytearray(stream)

    @abstractmethod
    def send_root(self):
        stream = io.BytesIO()
        self.root.serialize_root(stream)
        return bytearray(stream)

    @abstractmethod
    @overload
    def send_root(self, hash):
        stream = io.BytesIO()
        self.root.serialize_root_from(hash, stream)
        return bytearray(stream)

    @abstractmethod
    def add_hash(self, hash):
        return self.root.add_leaf(hash=hash, timestamp=Timestamp.get())

    @abstractmethod
    def get_branch(self):
        return self.root

    @abstractmethod
    def send_up(self, branch):  # TODO
        return Commons.cnt

    @abstractmethod
    def load_branch(self, stream):
        b = Branch(stream)
        if b.verify():
            return b
        else:
            return Branch()


class MidNode(Server, ABC):
    pass


class LowNode(Server, ABC):
    pass


class CoreNode(Server, ABC):
    pass
