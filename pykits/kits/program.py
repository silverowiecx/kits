import hashlib
import os
import random
import time
from sys import stdout
from threading import Thread

from kits.UI import interactive_node
from kits.classes import Timestamp, Branch, Sha256, Leaf
from kits.communication.server import NodeConfig, Server, Role
from kits.database import database


class Program:

    def __init__(self):
        self.root = Branch()
        self.n = 0
        self.hasze, self.hasze2 = list(), list()
        self.jeden2 = True
        self.jeden1 = True

    @staticmethod
    def rts():
        return bytearray(
            (int.from_bytes(Timestamp.get(), 'little') - random.randint(0, 1000000000)).to_bytes(4, 'little'))

    def random_branch_of_branches(self, branches, n):
        b = Branch()
        if n > 0:
            for i in range(branches):
                b.add_branch(self.random_branch_of_branches(1, n - 1), Program.rts())
        else:
            b.add_branch(self.random_branch(100), Program.rts())
        if self.jeden1:
            self.jeden1 = False
            self.hasze2.append(b.root_hash)
        return b

    def random_branch(self, leaves):
        b = Branch()
        for i in range(leaves):
            buf = bytearray()
            for x in range(64):
                buf.append(random.randint(0, 255))
            h = bytearray(hashlib.sha256(buf).digest())
            timestamp = Program.rts()
            b.add_leaf(hash=h, timestamp=timestamp)
            p = (h, timestamp)
            self.hasze.append(p)
        if self.jeden2:
            self.jeden2 = False
            self.hasze2.append(b.root_hash)
        return b

    def save(self, branch, fname):
        print("saving...")
        with open(fname, "wb") as file:
            branch.serialize(stream=file)
        print("saved: {}".format(fname))

    def load(self, fname):
        print("restoring: {}".format(fname))
        with open(fname, "rb") as file:
            return Branch(stream=file)

    def save_hashes(self, hashes, filename="hashes.txt"):
        with open(filename, "w") as f:
            for h in hashes:
                f.write(h[0].hex())
                f.write("\n")
                f.write(h[1].hex())
                f.write("\n")

    @staticmethod
    def partial_root():
        b = Branch()
        for i in range(1, 10):
            rts = Program.rts()
            hash = bytearray(32)
            b.add_leaf(hash, rts)
        with open('root', 'w+b') as f:
            b.serialize_root(f)
        print("TLC: {}; RH: {}".format(b.total_leaf_count, b.root_hash.hex()))
        root = b.root_hash
        for i in range(1, 10):
            rts = Program.rts()
            hash = bytearray(32)
            b.add_leaf(hash, rts)
        with open('root2', 'w+b') as f:
            b.serialize_root_from(root, stream=f)
        root = b.root_hash
        print("TLC2: {}; RH: {}".format(b.total_leaf_count, b.root_hash.hex()))
        b = Branch()
        with open('root', 'r+b') as f:
            b = Branch.only_root(stream=f)
        print("TLC: {}; RH: {}".format(b.total_leaf_count, b.root_hash.hex()))
        with open('root2', 'r+b') as f:
            rh = b.join_root(stream=f)
        if rh != root:
            print("OOPS! FRH: {}; expected: {}".format(rh.hex(), root.hex()))
        print("TLC2: {}; RH: {}".format(b.total_leaf_count, b.root_hash.hex()))
        print("Root verify succeeded: {}".format(b.verify()))

    @staticmethod
    def partial_root_db():
        b = Branch()
        c = database.connect()
        for i in range(1, 10):
            rts = Program.rts()
            hash = bytearray(32)
            b.add_leaf(hash, rts)
        database.serialize_branch_object(c, b)
        c.commit()
        print("TLC: {}; RH: {}".format(database.total_leaf_count(c), database.get_root_hash(c).hex()))
        root = database.get_root_hash(c)
        with open('root3', 'w+b') as f:
            database.serialize_tree(f)
        for i in range(1, 10):
            rts = Program.rts()
            hash = bytearray(32)
            database.add_leaf(c, hash=hash, timestamp=rts)
        print("TLC2: {}; RH: {}".format(database.total_leaf_count(c), database.get_root_hash(c).hex()))
        with open('root4', 'w+b') as f:
            database.serialize_root_from(c, f, root)
        root = database.get_root_hash(c)
        statement = "DELETE FROM LEAF;"
        cur = c.cursor()
        cur.execute(statement)

        with open('root3', 'r+b') as f:
            database.join_root(c, f)
        print("TLC: {}; RH: {}".format(database.total_leaf_count(c), database.get_root_hash(c).hex()))

        with open('root4', 'r+b') as f:
            rh = database.join_root(c, f)
        if rh != root:
            print("OOPS! FRH: {}; expected: {}".format(rh.hex(), root.hex()))
        print("TLC2: {}; RH: {}".format(database.total_leaf_count(c), database.get_root_hash(c).hex()))
        print("Root verify succeeded: {}".format(database.verify()))

    def save_ones(self):
        for hasz in self.hasze2:
            hexs = hasz.hex()
            print("saving: {}".format(hexs))
            with open(hexs, "w+b") as f:
                self.root.serialize_branch(hasz, f)

    def testy_1(self):
        n = 0
        n += 1
        print("Letsgo!")
        root = Branch()
        for i in range(10):
            root.add_branch(self.random_branch(20 + i), Program.rts())
        print("hashes count: {}".format(len(self.hasze)))
        print("Search test...")
        for i in range(1, 4):
            h = random.randint(0, len(self.hasze))
            hash = (self.hasze[h])[0]
            ts = int.from_bytes((self.hasze[h])[1], 'little')
            print("For {} i'm searching for: {}".format(hash.hex(), ts))
            t = root.find(hash, root)
            ts2 = int.from_bytes(t, 'little')
            print("For {} i got: {}".format(hash.hex(), ts2))
            print("Branch verify search: {}".format(Branch.verify(self.root.proof_find(hash, Branch()))))
            print(Branch.path)
            Branch.path.clear()
            if ts != ts2:
                print("Failed!")
            else:
                print("Passed!")

    def search_check(self):
        print("Search test...")
        for i in range(3):
            h = random.randint(0, len(self.hasze))
            hash = self.hasze[h][0]
            ts = int.from_bytes((self.hasze[h])[1], 'little')
            print("For {} i'm searching for: {}".format(hash.hex(), ts))
            t = self.root.find(hash=hash)
            print(t)
            ts2 = int.from_bytes(t, 'little')
            print("For {} i got: {}".format(hash.hex(), ts2))
            print("Branch verify search: {}".format(Branch.verify(self.root.proof_find(hash))))
            print(Branch.path)
            Branch.path.clear()
            b = self.root.proof_find(hash)
            if not b.verify():
                print("Fail verify!")
            if ts != int.from_bytes(b.find(hash), 'little'):
                print("Failed!")
            else:
                print("Passed!")

    def moar(self):
        for i in range(10):
            stdout.write("\rbranch: {} ".format(i))
            self.root.add_branch(branch=self.random_branch_of_branches(20, 4), timestamp=Program.rts())
        print("hashes count: {}".format(len(self.hasze)))

    def deterministic(self):
        root = Branch()
        to_byte = bytearray(os.urandom(32))
        hasz = int.from_bytes(to_byte, 'little')
        timestamp = 140000
        for i in range(10):
            l1 = Branch()
            for j in range(20):
                low = Branch()
                for l in range(100):
                    hasz -= 1
                    timestamp += 1
                    h = hasz.to_bytes(32, 'little')
                    t = timestamp.to_bytes(4, 'little')
                    low.add_leaf(h, t)
                    self.hasze.append((h, t))
                timestamp += 1
                t = timestamp.to_bytes(4, 'little')
                l1.add_branch(low, t)
            timestamp += 1
            t = timestamp.to_bytes(4, 'little')
            root.add_branch(l1, t)

        return root

    def search_full(self, root=None):
        if root is None:
            tree = self.root
        else:
            tree = root
        for o in range(2):
            p, q = 0, 0
            print("loop no. {}".format(o))
            i = 0
            for pair in self.hasze:
                i += 1
                b = tree.proof_find(hash=pair[0])
                if not b.verify():
                    p += 1
                ts = b.find(pair[0])
                if ts == pair[1]:
                    q += 1
                else:
                    p += 1
                stdout.write("\r #success= {} #fails= {}".format(q, p))
        print("done!")

    def search_full_db(self, c):
        for o in range(2):
            p, q = 0, 0
            print("loop no. {}".format(o))
            i = 0
            for pair in self.hasze:
                i += 1
                b = database.proof_find(c, hash=pair[0])
                if not b.verify():
                    p += 1
                ts = b.find(pair[0])
                if ts == pair[1]:
                    q += 1
                else:
                    p += 1
                stdout.write("\r #success= {} #fails= {}".format(q, p))
        print("done!")

    def clear_db(self, c):
        command = "Delete from leaf where id_leaf>0;"
        cursor = c.cursor()
        cursor.execute(command)
        c.commit()

    def search_check_db(self, c):
        print("Search check DB...")
        for i in range(3):
            h = random.randint(0, len(self.hasze))
            hash = self.hasze[h][0]
            ts = int.from_bytes((self.hasze[h])[1], 'little')
            print("For {} i'm searching for: {}".format(hash.hex(), ts))
            t = database.find(c, hash)
            ts2 = int.from_bytes(t, 'little')
            print("For {} i got: {}".format(hash.hex(), ts2))
            print("Branch verify search: {}".format(Branch.verify(self.root.proof_find(hash))))
            print(Branch.path)
            Branch.path.clear()
            b = database.proof_find(c, hash)
            if not b.verify():
                print("Verify failed!")
            if ts != int.from_bytes(b.find(hash), 'little'):
                print("Failed!")
            else:
                print("Passed!")


def task(server):
    print(server.clients[random.randint(0, len(server.clients) - 1)].get_root().verify())


def main():
    interactive_node()

    p = Program()
    p.clear_db(database.connect())
    p.partial_root_db()
    p.clear_db(database.connect())
    # time.sleep(100)
    p.moar()
    # p.partial_root()
    # configs = list()
    # for i in range(10):
    #     configs.append(NodeConfig(10000 + i, Role.MID, "node{}".format(i)))
    # for i in range(5):
    #     configs.append(NodeConfig(11000 + i, Role.CORE, "node{}".format(i + 11)))
    # for i in range(10):
    #     for j in range(10):
    #         if i != j:
    #             configs[i].side.append(configs[j])
    #     for j in range(10, 15):
    #         configs[i].higher.append(configs[j])
    # for i in range(10, 15):
    #     for j in range(10):
    #         configs[i].lower.append(configs[j])
    #     for j in range(10, 15):
    #         if i != j:
    #             configs[i].side.append(configs[j])
    #
    # servers = list()
    # for i in range(len(configs)):
    #     servers.append(Server(configs[i], branch=p.root))
    #     servers[i].start()
    #
    # for server in servers:
    #     server.connect_to_all()
    #
    # for i in range(15):
    #     t = Thread(target=task(servers[i]))
    #     t.start()

    config1 = NodeConfig(12002, role=Role.MID, to_hash="node1")
    config2 = NodeConfig(12003, role=Role.MID, to_hash="node2")
    config1.side.append(config2)
    config2.side.append(config1)

    server1 = Server(config1, branch=p.root)
    server2 = Server(config2)
    server1.start()
    server2.start()

    server2.connect_to_all()
    server1.clients[0].send_branch(server1.root)
    print(">>>", server2.root.verify())
    print(">>>", server2.root.serialized_size)

    server1.clients[0].send_hash(Sha256.to_hash("node1"))

    print(">>>", server1.root.verify())

    b = server1.clients[0].get_root()
    print(">>>", b.verify())
    print(">>>", b.total_leaf_count)


""""
    # p.jeden2 = True
    # p.jeden1 = True
    # c = database.connect()
    # # Branch.print_subtree = True
    # # with open('branches', 'w') as from_hash:
    # #     from_hash.write(repr(p.root))
    # print("saving hashes...")
    # p.save_hashes(p.hasze)
    # print("hashes are saved")
    # p.save(p.root, "out.byte")
    # # db.serialize_tree(p.root)
    # with open("out.byte", "rb") as file:
    #     c = database.connect()
    #     database.serialize_from_file(c, file)
    #     c.commit()
    #     c.close()
    # with open("out2.byte", "wb") as file:
    #     database.deserialize_tree(file)
    # print("DATABASE ROOT VERIFY: {}".format(database.verify()))
    # c = database.connect()
    # print("total leaf count db: {}".format(db.total_leaf_count(c)))
    # p.search_full_db(c)

    # new_branch = db.deserialize_tree()
    # p.save(new_branch, "out2.byte")
    # p.root = new_branch
    # # p.moar()
    # p.search_full()
    # p.search_check()
    #
    # # p.save_ones()
    # p.hasze.clear()
    # p.hasze2.clear()
    # #
    # # p.partial_root()
    # p.testy_1()
    # b = p.load("out2.byte")
    # print("Root verify succeeded: {}".format(b.verify()))
    # print("Loaded= Leaves: {}, Roothash: {}, Branches: {}"
    #       .format(len(b.leaves), b.root_hash.hex(), len(b.branches)))
    # p.save(b, "out2.byte")
"""
